
    create table jpa.Catalog (
        DATA_NOTEI date not null,
        ID_OBIECT integer not null,
        ID_STUDENT integer not null,
        nota integer not null,
        NR_PREZENTE integer,
        catalog_ID_OBIECT integer,
        catalog_ID_STUDENT integer,
        primary key (DATA_NOTEI, ID_OBIECT, ID_STUDENT)
    );

    create table jpa.Evaluare (
        ID_EVALUARE integer not null auto_increment,
        TIP_EVALUARE varchar(255),
        primary key (ID_EVALUARE)
    );

    create table jpa.Evaluare_Obiect (
        Evaluare_ID_EVALUARE integer not null,
        obiect_ID_OBIECT integer not null
    );

    create table jpa.Grupa (
        ID_GRUPA integer not null auto_increment,
        NUME_GRUPA varchar(255),
        primary key (ID_GRUPA)
    );

    create table jpa.News (
        ID_NEWS integer not null auto_increment,
        continut longtext,
        DATA_APARITIEI date,
        titlu varchar(255),
        primary key (ID_NEWS)
    );

    create table jpa.Obiect (
        ID_OBIECT integer not null auto_increment,
        credite integer not null,
        ID_EVALUARE integer,
        ID_PROFESOR integer,
        nume varchar(255),
        prof_ID_PROFESOR integer,
        primary key (ID_OBIECT)
    );

    create table jpa.Obiect_prezenta (
        Obiect_ID_OBIECT integer not null,
        prezenta varchar(255)
    );

    create table jpa.Orar (
        ID_OBIECT integer not null,
        data date,
        ID_GRUPA integer,
        INTERVAL_ORAR varchar(255),
        SALA_CURS integer,
        orar_ID_OBIECT integer,
        orar_ID_GRUPA integer,
        primary key (ID_OBIECT)
    );

    create table jpa.Profesor (
        ID_PROFESOR integer not null auto_increment,
        email varchar(255),
        nume varchar(255),
        prenume varchar(255),
        SITE_PERSONAL varchar(255),
        primary key (ID_PROFESOR)
    );

    create table jpa.Student (
        ID_STUDENT integer not null auto_increment,
        AN_STUDIU integer,
        email varchar(255),
        ID_GRUPA integer,
        NR_MATRICOL varchar(255),
        nume varchar(255),
        prenume varchar(255),
        studenti_ID_GRUPA integer,
        primary key (ID_STUDENT)
    );

    create table jpa.Usergroup (
        ID_GRUP integer not null auto_increment,
        groupname varchar(255),
        password varchar(255),
        username varchar(255),
        primary key (ID_GRUP)
    );

    create table jpa.evaluare_obiect (
        Obiect_ID_OBIECT integer not null,
        evaluare_ID_EVALUARE integer not null
    );

    alter table jpa.Catalog 
        add index FK8457F7F9C3124FF6 (catalog_ID_STUDENT), 
        add constraint FK8457F7F9C3124FF6 
        foreign key (catalog_ID_STUDENT) 
        references jpa.Student (ID_STUDENT);

    alter table jpa.Catalog 
        add index FK8457F7F9CD53A240 (catalog_ID_OBIECT), 
        add constraint FK8457F7F9CD53A240 
        foreign key (catalog_ID_OBIECT) 
        references jpa.Obiect (ID_OBIECT);

    alter table jpa.Evaluare_Obiect 
        add index FKBF7497447F987678 (Evaluare_ID_EVALUARE), 
        add constraint FKBF7497447F987678 
        foreign key (Evaluare_ID_EVALUARE) 
        references jpa.Evaluare (ID_EVALUARE);

    alter table jpa.Evaluare_Obiect 
        add index FKBF7497445CD1E447 (obiect_ID_OBIECT), 
        add constraint FKBF7497445CD1E447 
        foreign key (obiect_ID_OBIECT) 
        references jpa.Obiect (ID_OBIECT);

    alter table jpa.Obiect 
        add index FK8C651B001B207394 (prof_ID_PROFESOR), 
        add constraint FK8C651B001B207394 
        foreign key (prof_ID_PROFESOR) 
        references jpa.Profesor (ID_PROFESOR);

    alter table jpa.Obiect_prezenta 
        add index FKDD8B9EC5CD1E447 (Obiect_ID_OBIECT), 
        add constraint FKDD8B9EC5CD1E447 
        foreign key (Obiect_ID_OBIECT) 
        references jpa.Obiect (ID_OBIECT);

    alter table jpa.Orar 
        add index FK25A17496A3295B (orar_ID_GRUPA), 
        add constraint FK25A17496A3295B 
        foreign key (orar_ID_GRUPA) 
        references jpa.Grupa (ID_GRUPA);

    alter table jpa.Orar 
        add index FK25A174574161BB (orar_ID_OBIECT), 
        add constraint FK25A174574161BB 
        foreign key (orar_ID_OBIECT) 
        references jpa.Obiect (ID_OBIECT);

    alter table jpa.Student 
        add index FKF3371A1B5B743261 (studenti_ID_GRUPA), 
        add constraint FKF3371A1B5B743261 
        foreign key (studenti_ID_GRUPA) 
        references jpa.Grupa (ID_GRUPA);

    alter table jpa.evaluare_obiect 
        add index FK317CF3447F987678 (evaluare_ID_EVALUARE), 
        add constraint FK317CF3447F987678 
        foreign key (evaluare_ID_EVALUARE) 
        references jpa.Evaluare (ID_EVALUARE);

    alter table jpa.evaluare_obiect 
        add index FK317CF3445CD1E447 (Obiect_ID_OBIECT), 
        add constraint FK317CF3445CD1E447 
        foreign key (Obiect_ID_OBIECT) 
        references jpa.Obiect (ID_OBIECT);
