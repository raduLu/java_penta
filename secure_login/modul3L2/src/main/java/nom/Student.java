package nom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;

@Entity
@EntityListeners(value = { EntityListener.class })
@NamedQueries({
        @NamedQuery(name = "QueryStudenti", query = "SELECT s FROM Student s "
                + "WHERE s.nrMatricol = :num"),
        @NamedQuery(name = "DeleteStudenti", query = "DELETE FROM Student s ") })
public class Student implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_STUDENT")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idStudent;

    @Column(name = "AN_STUDIU")
    private int anStudiu;

    private String email;

    @Column(name = "ID_GRUPA")
    private int idGrupa;

    @Column(name = "NR_MATRICOL")
    private String nrMatricol;

    private String nume;

    private String prenume;

    @OneToMany(fetch = EAGER, cascade = ALL)
    @JoinColumn
    private List<Catalog> catalog=new ArrayList<Catalog>();

    public Student() {
    }

    public int getIdStudent() {
        return this.idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public int getAnStudiu() {
        return this.anStudiu;
    }

    public void setAnStudiu(int anStudiu) {
        this.anStudiu = anStudiu;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdGrupa() {
        return this.idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }

    public String getNrMatricol() {
        return this.nrMatricol;
    }

    public void setNrMatricol(String nrMatricol) {
        
            this.nrMatricol=nrMatricol;
        
    }

    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return this.prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public List<Catalog> getCatalog() {
        return catalog;
    }

    public void setCatalog(List<Catalog> catalog) {
        this.catalog = catalog;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + anStudiu;
		result = prime * result + ((catalog == null) ? 0 : catalog.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + idGrupa;
		result = prime * result + idStudent;
		result = prime * result
				+ ((nrMatricol == null) ? 0 : nrMatricol.hashCode());
		result = prime * result + ((nume == null) ? 0 : nume.hashCode());
		result = prime * result + ((prenume == null) ? 0 : prenume.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (anStudiu != other.anStudiu)
			return false;
		if (catalog == null) {
			if (other.catalog != null)
				return false;
		} else if (!catalog.equals(other.catalog))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (idGrupa != other.idGrupa)
			return false;
		if (idStudent != other.idStudent)
			return false;
		if (nrMatricol == null) {
			if (other.nrMatricol != null)
				return false;
		} else if (!nrMatricol.equals(other.nrMatricol))
			return false;
		if (nume == null) {
			if (other.nume != null)
				return false;
		} else if (!nume.equals(other.nume))
			return false;
		if (prenume == null) {
			if (other.prenume != null)
				return false;
		} else if (!prenume.equals(other.prenume))
			return false;
		return true;
	}

    
   

    

}