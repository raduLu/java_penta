package nom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.PERSIST;



@Entity
@EntityListeners(value = { EntityListener.class })
public class Profesor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="ID_PROFESOR")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idProfesor;

    private String email;

    private String nume;

    private String prenume;

    @Column(name="SITE_PERSONAL")
    private String sitePersonal;
    
    @OneToMany(mappedBy = "prof", cascade = { ALL, PERSIST })
      private List<Obiect> obiecte=new ArrayList<Obiect>();

    public Profesor() {
    }

    public int getIdProfesor() {
        return this.idProfesor;
    }

    public void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return this.prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getSitePersonal() {
        return this.sitePersonal;
    }

    public void setSitePersonal(String sitePersonal) {
        this.sitePersonal = sitePersonal;
    }

    public List<Obiect> getObiecte() {
        return obiecte;
    }

    public void setObiecte(List<Obiect> obiecte) {
        this.obiecte = obiecte;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + idProfesor;
		result = prime * result + ((nume == null) ? 0 : nume.hashCode());
		result = prime * result + ((obiecte == null) ? 0 : obiecte.hashCode());
		result = prime * result + ((prenume == null) ? 0 : prenume.hashCode());
		result = prime * result
				+ ((sitePersonal == null) ? 0 : sitePersonal.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profesor other = (Profesor) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (idProfesor != other.idProfesor)
			return false;
		if (nume == null) {
			if (other.nume != null)
				return false;
		} else if (!nume.equals(other.nume))
			return false;
		if (obiecte == null) {
			if (other.obiecte != null)
				return false;
		} else if (!obiecte.equals(other.obiecte))
			return false;
		if (prenume == null) {
			if (other.prenume != null)
				return false;
		} else if (!prenume.equals(other.prenume))
			return false;
		if (sitePersonal == null) {
			if (other.sitePersonal != null)
				return false;
		} else if (!sitePersonal.equals(other.sitePersonal))
			return false;
		return true;
	}


}