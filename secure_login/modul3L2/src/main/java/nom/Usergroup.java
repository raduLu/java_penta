package nom;

// Generated Aug 27, 2014 3:40:09 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;


@Entity

public class Usergroup implements java.io.Serializable {

	
	private static final long serialVersionUID = -1273223563656399454L;
	private Integer idGrup;
	private String groupname;
	
	private String username;

	public Usergroup() {
	}

	public Usergroup(String groupname, String password, String username) {
		this.groupname = groupname;
		
		this.username = username;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_GRUP", unique = true, nullable = false)
	public Integer getIdGrup() {
		return this.idGrup;
	}

	public void setIdGrup(Integer idGrup) {
		this.idGrup = idGrup;
	}

	@Column(name = "groupname")
	public String getGroupname() {
		return this.groupname;
	}

	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}

	

	@Column(name = "username")
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
