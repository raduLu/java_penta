package test;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class ProfesorForm {

	private String numeProfesor="";
	private String prenumeProfesor="";
	private String email="";
	private String site="";
	private String numeForm="";
	
	public void reset(){
		this.numeProfesor="";
		this.email="";
		this.prenumeProfesor="";
		this.site="";
	}
	
	
	public String getNumeProfesor() {
		return numeProfesor;
	}
	public void setNumeProfesor(String numeProfesor) {
		this.numeProfesor = numeProfesor;
	}
	public String getPrenumeProfesor() {
		return prenumeProfesor;
	}
	public void setPrenumeProfesor(String prenumeProfesor) {
		this.prenumeProfesor = prenumeProfesor;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getNumeForm() {
		return numeForm;
	}
	public void setNumeForm(String numeForm) {
		this.numeForm = numeForm;
	}
	
	
}
