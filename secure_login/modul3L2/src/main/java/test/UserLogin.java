package test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.data.SortEvent;

import nom.Catalog;
import nom.News;
import nom.Obiect;
import nom.Profesor;
import service.DatabaseInitService;
import service.UserService;

@ManagedBean(eager = true)
public class UserLogin {

	// private static final long serialVersionUID = 1L;
	

	private String nrMatricol;
	private String email;

	private List<StudentForm> s;
	
	private List<Profesor> profesori;
	private List<Catalog> catalog = new ArrayList<Catalog>();
	private List<String> grupe;
	
	private List<NewsBean>selectionNews=new ArrayList<NewsBean>();
	private List<CatalogBean> c1 = new ArrayList<CatalogBean>();
	private List<Obiect>obiectePredate=new ArrayList<Obiect>();
	private boolean loginVar;

	@ManagedProperty(value = "#{actionLinkClass}")
	private ActionLinkClass service;

	@PostConstruct
	public void init() {
		profesori = UserService.creareListaProfesori();
		
		grupe = UserService.creareListaGrupe();
		
	}
	public String login(){
		
		loginVar=true;
		
		return null;
	}
	public String createShortNews(String continut){
		
		//String continut=stiri.get(row).getStire().getContinut();
		return (continut.substring(0, continut.length()/5) + "...");
	}
	public void cancel(){
		service.creareCatalog();
	}
	public String updateProfesor(){
		
		UserService.updateProfesor(service.getProf());
		profesori=UserService.creareListaProfesori();
		return null;
	}
	public void addProfesor(){
		UserService.adaugaProfesor(service.getProf());
		service.setProfesori(UserService.creareListaProfesori());
		service.reset();
	}
	public void updateCatalog() {
		for (CatalogBean ci : c1) {
			service.getCatalog().get(service.getCatalog().indexOf(ci))
					.setSelected(true);
		}
		c1 = new ArrayList<CatalogBean>();
	}

	public void aplliedAction() {
		for (CatalogBean ci : service.getCatalog()) {
			ci.setSelected(false);
		
		}
		UserService.aplliedAction(service.getCatalog());
		service.creareCatalog();
	}

	public void addCatalog() {
		service.setCatalogAction("add");
		CatalogBean catalogNou = new CatalogBean();
		catalogNou.setSelected(true);
		catalogNou.setC(new Catalog());
		service.getCatalog().add(catalogNou);
		
	}
	

	public void removeCatalog() {
		for (CatalogBean ci : c1) {
			service.getCatalog().remove(ci);
			
		}
		c1 = new ArrayList<CatalogBean>();
	}

	public void listenerTabView(SelectEvent event) {
		Date date1 = (Date) event.getObject();
		service.setZilele(new ArrayList<String>());
		service.setZilele(UserService.generareListaDate(date1));
		SimpleDateFormat formatDate = new SimpleDateFormat("EEEE dd/MM/YYYY");
		service.setTabViewIndex(String.valueOf(service.getZilele().indexOf(
				formatDate.format(date1))));
		service.setOrar(UserService.listareOrar(service.getGrupaAleasa(),
				formatDate.format(service.getDataAleasa())));
		
		service.setDataAleasa(new Date());

	}

	public void showOrar() {

		service.setDataAleasa(new Date());

		service.setZilele(new ArrayList<String>());
		System.out.println(service.getDataAleasa());
		service.setZilele(UserService.generareListaDate(service.getDataAleasa()));
		SimpleDateFormat formatDate = new SimpleDateFormat("EEEE dd/MM/YYYY");
		service.setTabViewIndex(String.valueOf(service.getZilele().indexOf(
				formatDate.format(service.getDataAleasa()))));
		service.setOrar(UserService.listareOrar(service.getGrupaAleasa(),
				formatDate.format(service.getDataAleasa())));

	}

	public void onTabChange(TabChangeEvent event) {

		// System.out.println(event.getTab().getTitle()+"celalalt");
		// System.out.println(service.getTabViewIndex()+"asta1");
		if (service.getTabViewIndex() != null) {
			service.setOrar(UserService.listareOrar(
					service.getGrupaAleasa(),
					service.getZilele().get(
							Integer.valueOf(service.getTabViewIndex())
									.intValue())));
			// System.out.println(service.getTabViewIndex()+"asta"+service.getZilele().get(Integer.valueOf(service.getTabViewIndex()).intValue()));
		}

	}

	public void findStudent(int row) {

		// service.sortData();
		service.setStudent(new StudentForm());
		service.setStudent(service.getS().get(row));
		System.out.println(service.getStudent().getNume());
		service.setNumarMatricol(service.getS().get(row).getNrMatricol());
		service.removeMessage("student", service.getStudent().getNume(),
				service.getStudent().getNrMatricol());
		// System.out.println(service.getMessage());

	}
	public void findProfesor(int row){
		service.setProf(service.getProfesori().get(row));
		
		service.removeMessage("profesor", service.getProf().getNume(),
				service.getProf().getPrenume());
	}
	public void removeProfesor(){
		UserService.removeProfesor(service.getProf());
		service.setProfesori(UserService.creareListaProfesori());
		
	}
	public void showObiectePredate(int row){
		findProfesor(row);
		obiectePredate=UserService.findObiectePredate(service.getProf());
		System.out.println(obiectePredate.size()+" "+obiectePredate.get(0).getCredite());
	}

	public String removeStudent() {

		UserService.removeStudent(service.getNumarMatricol());
		service.setS(UserService.creareListaStudentiForm(service
				.getGrupaAleasa()));
		return null;
	}

	public String updateStudent() {
		UserService.updateStudent(service.getStudent(),
				service.getNumarMatricol());
		service.setS(UserService.creareListaStudentiForm(service
				.getGrupaAleasa()));
		return null;
	}
	public void updateNews(){
		for (NewsBean ind:selectionNews){
			
			ind.setSelected(true);
		}
		selectionNews= new ArrayList<NewsBean>();
	}
	public void removeNews(){
		for (NewsBean ind:selectionNews){
			
			ind.setRemove(true);
		}
		selectionNews= new ArrayList<NewsBean>();
	}
	public void addNews(){
		NewsBean stireNoua= new NewsBean();
		stireNoua.setSelected(true);
		stireNoua.setStire(new News());
		stireNoua.getStire().setDataAparitiei(new Date());
		
		service.getStiri().add(stireNoua);
	}
	public void saveNews(){
		for (NewsBean ind:service.getStiri()){
			
			ind.setSelected(false);
		}
		UserService.aplliedActionNews(service.getStiri());
		service.setStiri(UserService.creareListaN());
	}
	public void cancelNews(){
		service.setStiri(UserService.creareListaN());
	}
	public void populate() {
		DatabaseInitService.initDatbase();
	}

	 public String logout() {
		    String result="second?faces-redirect=true";
		     
		    FacesContext context = FacesContext.getCurrentInstance();
		    HttpServletRequest request = (HttpServletRequest)context.getExternalContext().getRequest();
		     
		    try {
		      request.logout();
		    } catch (ServletException e) {
		      
		      result = "failedlogin?faces-redirect=true";
		    }
		     
		    return result;
		  }
	

	public Comparator<Profesor> ProfesorNameComparatorAscending = new Comparator<Profesor>() {

		@Override
		public int compare(Profesor p1, Profesor p2) {
			return p1.getNume().compareTo(p2.getNume())==0?p1.getPrenume().compareTo(p2.getPrenume()):p1.getNume().compareTo(p2.getNume());
		}
	};
	public Comparator<Profesor> ProfesorNameComparatorDescending = new Comparator<Profesor>() {

		@Override
		public int compare(Profesor p1, Profesor p2) {
			return p2.getNume().compareTo(p1.getNume())==0?p2.getPrenume().compareTo(p1.getPrenume()):p2.getNume().compareTo(p1.getNume());
		}
	};

	public void onSort(SortEvent event) {
		
		if (event.isAscending()) {
			Collections.sort(service.getProfesori(),
					ProfesorNameComparatorAscending);
		} else {
			Collections.sort(service.getProfesori(),
					ProfesorNameComparatorDescending);
		}

	}

	public void showUpdateProfesor(int row) {
		
		service.setProf(service.getProfesori().get(row));
		
	}

	public String getNrMatricol() {
		return nrMatricol;
	}

	public void setNrMatricol(String nrMatricol) {
		this.nrMatricol = nrMatricol;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<StudentForm> getS() {
		// if (FacesContext.getCurrentInstance().getRenderResponse()) {
		// init();
		// }
		return s;
	}

	public void setS(List<StudentForm> s) {
		// init();
		this.s = s;
	}

	

	public List<Catalog> getCatalog() {
		return catalog;
	}

	public void setCatalog(List<Catalog> catalog) {
		this.catalog = catalog;
	}

	public ActionLinkClass getService() {
		return service;
	}

	public void setService(ActionLinkClass service) {
		this.service = service;
	}

	public List<Profesor> getProfesori() {
		return profesori;
	}

	public void setProfesori(List<Profesor> profesori) {
		this.profesori = profesori;
	}

	public List<String> getGrupe() {
		return grupe;
	}

	public void setGrupe(List<String> grupe) {
		this.grupe = grupe;
	}

	public List<CatalogBean> getC1() {
		return c1;
	}

	public void setC1(List<CatalogBean> c1) {
		this.c1 = c1;
	}
	
	public List<NewsBean> getSelectionNews() {
		return selectionNews;
	}
	public void setSelectionNews(List<NewsBean> selectionNews) {
		this.selectionNews = selectionNews;
	}
	public List<Obiect> getObiectePredate() {
		return obiectePredate;
	}
	public void setObiectePredate(List<Obiect> obiectePredate) {
		this.obiectePredate = obiectePredate;
	}
	public boolean isLoginVar() {
		return loginVar;
	}
	public void setLoginVar(boolean loginVar) {
		this.loginVar = loginVar;
	}

}
