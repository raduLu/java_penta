package test;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import service.UserService;

@ManagedBean
public class StudentForm {

	private String nrMatricol = "";
	private String nume = "";
	private String numeBaza = "";
	private String prenume = "";
	private int an;
	private String orar = "";
	private String catalog = "";
	private String grupa = "";
	private String email = "";

	@ManagedProperty(value = "#{actionLinkClass}")
	private ActionLinkClass service;

	public StudentForm() {

	}

	public String getNrMatricol() {
		return nrMatricol;
	}

	public void setNrMatricol(String nrMatricol) {
		this.nrMatricol = nrMatricol;
	}

	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public int getAn() {
		return an;
	}

	public void setAn(int an) {
		this.an = an;
	}

	public String getOrar() {
		return orar;
	}

	public void setOrar(String orar) {
		this.orar = orar;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getGrupa() {
		return grupa;
	}

	public void setGrupa(String grupa) {
		this.grupa = grupa;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumeBaza() {
		return numeBaza;
	}

	public void setNumeBaza(String numeBaza) {
		this.numeBaza = numeBaza;
	}

	public String add() {
		this.grupa = service.getGrupaAleasa();
		FacesContext context = FacesContext.getCurrentInstance();
		if (service.getGrupaAleasa() != null) {

			if (context.getMessageList().size() > 0) {
				return (null);
			} else   {
						String result = UserService.adaugaStudent(this);
						
				
						if (result.equals("student existent")) {
							System.out.println(result);
								context.addMessage(null, new FacesMessage("Student existent"));
								 return(null);
						} else {
							service.setS(UserService.creareListaStudentiForm(service.getGrupaAleasa()));
							RequestContext.getCurrentInstance().execute("dlg4.hide()");
							}
				
						}
				RequestContext.getCurrentInstance().execute("dlg4.hide()");
		} else {
			context.addMessage(null, new FacesMessage("Grupa inexistenta"));
		}
		  return(null);
    
	}

	public ActionLinkClass getService() {
		return service;
	}

	public void setService(ActionLinkClass service) {
		this.service = service;
	}

}
