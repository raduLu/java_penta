package test;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Stform implements Serializable {

	
	private static final long serialVersionUID = 1L;
	  private final int OPT1 = 1;
	   private final int OPT2 = 2;
	   private final int OPT3 = 3;
	   private final int OPT4 = 4;
	   private final int OPT5 = 5;
	   private final int OPT6 = 6;
	   private final int OPTo = 0;
	   private int con=-1;
	  
	   
	   private int selectedOption;
	   private String icon;
	   private boolean selectedOnOff;
	   
	   public Stform() {
		   selectedOption = OPTo;
		   selectedOnOff=false;
		   icon="ui-icon-carat-1-s";		  
	   }
	   
	   public void setSelectedOptionOnOff(){
		   selectedOption = OPTo;
		 selectedOnOff=!selectedOnOff;
		 if (icon.equals("ui-icon-carat-1-s")){
			 icon="ui-icon-carat-1-n";
		 }
		 else{
			 icon="ui-icon-carat-1-s";
		 }
	   }

	   public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public int getSelectedOption() {
	      return selectedOption;
	   }

	   public void setSelectedOption(int selectedOption) {
		   selectedOnOff=false;
		   icon="ui-icon-carat-1-s";	
		   if(selectedOption==3)
		   {
			   
			   con=-1;
		   }
	      this.selectedOption = selectedOption;
	      
	   }

	   public int getOPT1() {
	      return OPT1;
	   }

	   public int getOPT2() {
	      return OPT2;
	   }

	public int getOPT3() {
		return OPT3;
	}
	

	public int getOPTo() {
		return OPTo;
	}

	
	
	public int getCon() {
		return con;
	}

	public void setCon(int con) {
		this.con = con;
	}

	public void ps(int row){
		if(con==-1){
		con=row;}
		else{
			con=-1;
		}
	}

	public boolean isSelectedOnOff() {
		return selectedOnOff;
	}

	public void setSelectedOnOff(boolean selectedOnOff) {
		this.selectedOnOff = selectedOnOff;
	}

	public int getOPT4() {
		return OPT4;
	}

	public int getOPT5() {
		return OPT5;
	}

	public int getOPT6() {
		return OPT6;
	}
	
	
}
