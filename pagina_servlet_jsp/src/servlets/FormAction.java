package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FormAction
 */
@WebServlet("/FormAction")
public class FormAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FormAction() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nume=request.getParameter("nume");
		String prenume=request.getParameter("prenume");
		String varsta=request.getParameter("varsta");
		response.setContentType("text/html");
	    request.setAttribute("nume", nume);
	    request.setAttribute("prenume", prenume);
	    request.setAttribute("varsta", varsta);
	    request.getRequestDispatcher("/raspuns.jsp").forward(request, response);
	}

}
