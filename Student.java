
import java.util.Scanner;
public  class Student{
	long id;
	public String nume;
	public String prenume;
	int varsta;
	int anDeStudiu;
	String facultate;
	private Scanner consola;
	public Student(){
		consola=new Scanner(System.in);
		System.out.print("Introduceti ID_Student:");
		this.id=consola.nextLong();
		System.out.println("Introduceti nume_Student :");
		this.nume=consola.next();
		System.out.println("Introduceti prenume_Student:");
		this.prenume=consola.next();
		System.out.println("Introduceti varsta_Student:");
		this.varsta=consola.nextInt();
		System.out.println("Introduceti an de studiu_Student:");
		this.anDeStudiu=consola.nextInt();
		System.out.println("Introduceti facultatea_Student:");
		this.facultate=consola.next();
		
	}
	@Override 
	public boolean equals(Object obj)
 	{
	if(this == obj)
		return true;
	if((obj == null) || (obj.getClass() != this.getClass()))
		return false;
	Student s=(Student)obj;
	return id==s.id && (facultate!=null && facultate.equals(s.facultate))&&(nume!=null && nume.equals(s.nume))&&(prenume!=null && prenume.equals(s.prenume));
 	}
public int hashCode()
	{
			int hash = 1;
			hash = 2 * hash +(int)id;
			hash = 2 * hash + (null == facultate ? 0 : facultate.hashCode());
			//hash = 2 * hash + (null == nume ? 0 : nume.hashCode());
			//hash = 2 * hash + (null == prenume ? 0 : prenume.hashCode());
			return hash;
	}
}