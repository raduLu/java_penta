package service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import test.CatalogBean;
import test.NewsBean;
import test.OrarForm;
import test.ProfesorForm;
import test.StudentForm;

import nom.Catalog;
import nom.News;
import nom.Obiect;
import nom.Orar;
import nom.Profesor;
import nom.Student;
import nom.Grupa;

public class UserService {
	private static EntityManager em;

	public static List<String> generareListaObiecte(String nume, String prenume) {
		List<String> listaObiecte = new ArrayList<String>();
		Student s = new Student();
		Obiect o = new Obiect();
		List<Catalog> catalogStudent = new ArrayList<Catalog>();
		Query query;
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		query = em
				.createQuery(
						"SELECT s FROM Student s WHERE s.nume=:v1 AND s.prenume=:v2")
				.setParameter("v1", nume).setParameter("v2", prenume);
		s = (Student) query.getSingleResult();
		if (!s.equals(null)) {
			catalogStudent = s.getCatalog();
		}

		for (Catalog c : catalogStudent) {
			query = em.createQuery(
					"SELECT o FROM Obiect o WHERE o.idObiect=:v1")
					.setParameter("v1", c.getIdObiect());
			o = (Obiect) query.getSingleResult();
			if (!o.equals(null)) {
				listaObiecte.add(o.getNume());
			}

		}

		em.getTransaction().commit();

		return listaObiecte;
	}

	public static List<String> generareListaDate(Date date1) {
		List<String> result = new ArrayList<String>();
		SimpleDateFormat formatDate = new SimpleDateFormat("EEEE dd/MM/YYYY");
		Calendar c = Calendar.getInstance();
		c.setTime(date1);

		for (int i = 0; i < 5; i++) {
			c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
			Date date = c.getTime();
			c.setTime(date);
			c.add(Calendar.DATE, i);
			date = c.getTime();
			result.add(formatDate.format(date));

		}
		return result;

	}

	public static String adaugaProfesor(ProfesorForm profesor) {

		Profesor p = new Profesor();
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		p.setNume(profesor.getNumeProfesor());
		p.setPrenume(profesor.getPrenumeProfesor());
		p.setEmail(profesor.getEmail());
		p.setSitePersonal(profesor.getSite());
		em.persist(p);
		em.getTransaction().commit();
		return "succes";

	}

	@SuppressWarnings("unchecked")
	public static List<NewsBean> creareListaNews() {

		List<News> n = new ArrayList<News>();
		List<NewsBean> data = new ArrayList<NewsBean>();
		NewsBean c = new NewsBean();
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em.createQuery("SELECT n FROM News n");
		n = query.getResultList();
		int i = 0;
		for (News ind : n) {
			c.setContinutNews(ind.getContinut());
			c.setContinutShortNews(ind.getContinut().substring(0, 90) + "...");

			data.add(c);
		}
		em.getTransaction().commit();
		em.close();
		return data;

	}

	public static List<OrarForm> listareOrar(String numeGrupa, String data) {

		List<Orar> o = new ArrayList<Orar>();
		List<Grupa> gr = new ArrayList<Grupa>();
		List<OrarForm> orar = new ArrayList<OrarForm>();
		OrarForm obiectOrar;

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em.createQuery(
				"SELECT g FROM Grupa g WHERE g.numeGrupa=:v").setParameter("v",
				numeGrupa);
		gr = (List<Grupa>) query.getResultList();
		SimpleDateFormat formatDate = new SimpleDateFormat("EEEE dd/MM/YYYY");
		if (!gr.isEmpty()) {
			o = gr.get(0).getOrar();
		}
		String numeObiect;
		for (Orar oIndex : o) {
			obiectOrar = new OrarForm();
			if (formatDate.format(oIndex.getData()).equals(data)) {
				numeObiect = (String) em
						.createQuery(
								"SELECT o.nume FROM Obiect o WHERE o.idObiect=:v")
						.setParameter("v", oIndex.getIdObiect())
						.getSingleResult();
				obiectOrar.setNumeObiect(numeObiect);
				obiectOrar.setData(formatDate.format(oIndex.getData()));
				obiectOrar.setInterval(oIndex.getIntervalOrar());
				obiectOrar.setSala(String.valueOf(oIndex.getSalaCurs()));
				orar.add(obiectOrar);
			}

		}
		em.getTransaction().commit();
		em.close();
		System.out.println(orar.size());
		return orar;

	}

	@SuppressWarnings("unchecked")
	public static List<CatalogBean> listareCatalog(String obiect) {
		List<Catalog> c = new ArrayList<Catalog>();
		List<CatalogBean> catalog = new ArrayList<CatalogBean>();
		Obiect ol = new Obiect();

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em
				.createQuery("SELECT o FROM Obiect o WHERE o.nume=:v1 ")
				.setParameter("v1", obiect);

		ol = (Obiect) query.getSingleResult();

		if (!ol.equals(null)) {
			c = ol.getCatalog();

		}

		
		int i = 0;
		for (Catalog cInd : c) {

			CatalogBean v = new CatalogBean();
			v.setId(String.valueOf(i++));
			v.setC(cInd);
			v.setSelected(false);
			catalog.add(v);

		}

		em.getTransaction().commit();
		em.close();
		return catalog;
	}

	public static List<CatalogBean> aplliedAction(String action,
			List<CatalogBean> modificari, String obiect) {
		List<CatalogBean> catalog = new ArrayList<CatalogBean>();
		List<Catalog> c = new ArrayList<Catalog>();
		List<Catalog> cModificari = new ArrayList<Catalog>();
		
		Obiect ol = new Obiect();
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em
				.createQuery("SELECT o FROM Obiect o WHERE o.nume=:v1 ")
				.setParameter("v1", obiect);

		ol = (Obiect) query.getSingleResult();

		if (!ol.equals(null)) {
			c = ol.getCatalog();
		

		}
		for(CatalogBean ind:modificari){
			cModificari.add(ind.getC());
		}
		System.out.println(c.size());
		for(Catalog ind:cModificari){
			c.remove(ind);
			System.out.println(action+"   "+c.contains(ind)+"  "+obiect);
		}
			
			
				
				int i = 0;
				for (Catalog cInd : c) {

					CatalogBean v = new CatalogBean();
					v.setId(String.valueOf(i++));
					v.setC(cInd);
					v.setSelected(false);
					catalog.add(v);

				}
			
			
		
		ol.setCatalog(c);
		em.merge(ol);
		em.getTransaction().commit();
		em.close();

		return catalog;

	}

	@SuppressWarnings("unchecked")
	public static List<ProfesorForm> creareListaProfesori() {
		List<Profesor> p = new ArrayList<Profesor>();
		List<ProfesorForm> profesorFormList = new ArrayList<ProfesorForm>();
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em.createQuery("SELECT p FROM Profesor p");
		p = (List<Profesor>) query.getResultList();
		if (!p.isEmpty()) {
			for (Profesor index : p) {
				ProfesorForm pf = new ProfesorForm();
				pf.setEmail(index.getEmail());
				pf.setNumeForm(index.getNume() + " " + index.getPrenume());
				pf.setNumeProfesor(index.getNume());
				pf.setPrenumeProfesor(index.getPrenume());
				pf.setSite(index.getSitePersonal());
				profesorFormList.add(pf);
			}
		}
		return profesorFormList;

	}

	@SuppressWarnings("unchecked")
	public static List<String> creareListaGrupe() {
		List<String> numeGrupe = new ArrayList<String>();
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em
				.createQuery("SELECT DISTINCT g.numeGrupa FROM Grupa g ");

		numeGrupe = (List<String>) query.getResultList();
		return numeGrupe;
	}

	@SuppressWarnings("unchecked")
	public static List<StudentForm> creareListaStudentiForm(String grupa) {
		List<StudentForm> stForm = new ArrayList<StudentForm>();
		List<Grupa> gr = new ArrayList<Grupa>();
		// System.out.println(grupa);
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em.createQuery(
				"SELECT g FROM Grupa g WHERE g.numeGrupa=:v1").setParameter(
				"v1", grupa);
		gr = (List<Grupa>) query.getResultList();

		if (!gr.isEmpty()) {
			for (Grupa g : gr) {
				for (Student s : g.getStudenti()) {
					StudentForm sf = new StudentForm();
					sf.setNrMatricol(s.getNrMatricol());
					sf.setAn(s.getAnStudiu());
					sf.setNume(s.getNume() + " " + s.getPrenume());
					sf.setNumeBaza(s.getNume());
					sf.setGrupa(g.getNumeGrupa());
					sf.setEmail(s.getEmail());
					sf.setPrenume(s.getPrenume());
					sf.setOrar(g.getNumeGrupa());
					sf.setCatalog("Go to ...");
					stForm.add(sf);
				}
			}
			em.getTransaction().commit();
			em.close();
		}
		return stForm;
	}

	public static String validUser(String nrMatricol, String email) {
		List<Student> st = new ArrayList<Student>();

		try {
			EntityManagerFactory emf = Persistence
					.createEntityManagerFactory("modul3L2");
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Query query = em
					.createQuery(
							"SELECT s FROM Student s WHERE s.nrMatricol=:v1 AND s.email=:v2")
					.setParameter("v1", nrMatricol).setParameter("v2", email);
			st = query.getResultList();

			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			// log the exception
		}

		if (st.size() == 1) {

			return "second?faces-redirect=true";
		}

		return "failedlogin?faces-redirect=true";

	}

	@SuppressWarnings("unchecked")
	public static String adaugaStudent(StudentForm studentForm) {
		List<Student> st = new ArrayList<Student>();
		Query query;
		boolean q1 = true, q2 = true;
		try {
			EntityManagerFactory emf = Persistence
					.createEntityManagerFactory("modul3L2");
			em = emf.createEntityManager();
			em.getTransaction().begin();
			query = em
					.createQuery(
							"SELECT s FROM Student s WHERE s.nume=:v1 AND s.prenume=:v2")
					.setParameter("v1", studentForm.getNume())
					.setParameter("v2", studentForm.getPrenume());
			q1 = !query.getResultList().isEmpty();
			System.out.println(q1);
			query = em.createQuery(
					"SELECT s FROM Student s WHERE  s.nrMatricol=:v1")
					.setParameter("v1", studentForm.getNrMatricol());
			q2 = !query.getResultList().isEmpty();
			System.out.println(q2);
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			// log the exception
		}

		if (q1 || q2) {

			return "student existent";
		} else {
			Student s = new Student();
			s.setAnStudiu(Integer.valueOf(studentForm.getAn()).intValue());
			s.setNrMatricol(studentForm.getNrMatricol());
			s.setNume(studentForm.getNume());
			s.setPrenume(studentForm.getPrenume());
			s.setEmail(studentForm.getEmail());
			List<Grupa> g = new ArrayList<Grupa>();
			EntityManagerFactory emf = Persistence
					.createEntityManagerFactory("modul3L2");
			em = emf.createEntityManager();
			em.getTransaction().begin();
			query = em.createQuery(
					"SELECT g FROM Grupa g WHERE g.numeGrupa=:v1")
					.setParameter("v1", studentForm.getGrupa());
			g = query.getResultList();
			Grupa gr = new Grupa();
			if (g.size() == 1) {

				gr = g.get(0);
				s.setIdGrupa(gr.getIdGrupa());
				gr.getStudenti().add(s);
				em.merge(gr);

			} else {
				gr.setNumeGrupa(studentForm.getGrupa());
				s.setIdGrupa(0);
				gr.getStudenti().add(s);
				em.persist(gr);
			}

		}
		em.getTransaction().commit();
		em.close();
		return "succes";

	}

	public static StudentForm findStudent(String nrMatricol) {
		List<Student> st = new ArrayList<Student>();

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em.createQuery(
				"SELECT s FROM Student s WHERE s.nrMatricol=:v1").setParameter(
				"v1", nrMatricol);

		st = query.getResultList();

		StudentForm result = new StudentForm();
		if (!st.isEmpty()) {
			String numeGrupa = (String) em
					.createNativeQuery(
							"SELECT grupa.NUME_GRUPA  FROM Grupa,Student WHERE grupa.ID_GRUPA=student.studenti_id_grupa AND student.NR_MATRICOL=?1")
					.setParameter(1, nrMatricol).getSingleResult();

			result.setAn(st.get(0).getAnStudiu());
			result.setEmail(st.get(0).getEmail());
			result.setGrupa(numeGrupa);
			result.setNrMatricol(nrMatricol);
			result.setNume(st.get(0).getNume());
			result.setPrenume(st.get(0).getPrenume());
		}
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@SuppressWarnings("unchecked")
	public static void removeStudent(String nrMatricol) {
		List<Student> st = new ArrayList<Student>();
		Grupa g = new Grupa();
		Catalog c = new Catalog();
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em.createQuery(
				"SELECT s FROM Student s WHERE s.nrMatricol=:v1").setParameter(
				"v1", nrMatricol);
		st = query.getResultList();
		if (!st.isEmpty()) {
			String numeGrupa = (String) em
					.createNativeQuery(
							"SELECT grupa.NUME_GRUPA  FROM Grupa,Student WHERE grupa.ID_GRUPA=student.studenti_id_grupa AND student.NR_MATRICOL=?1")
					.setParameter(1, nrMatricol).getSingleResult();
			g = (Grupa) em
					.createQuery("SELECT g FROM Grupa g WHERE g.numeGrupa=:v1")
					.setParameter("v1", numeGrupa).getSingleResult();
			g.getStudenti().remove(g.getStudenti().indexOf(st.get(0)));
		}
		em.merge(g);
		query = em.createQuery("DELETE FROM Student s WHERE s.nrMatricol=:v1")
				.setParameter("v1", nrMatricol);
		query.executeUpdate();

		em.getTransaction().commit();
		em.close();

	}

	public static void updateStudent(StudentForm studentUpdate,
			String nrMatricol) {

		Student updatedStudent = new Student();
		Student toBeUpdatedStudent = new Student();
		Grupa g = new Grupa();
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		em.getTransaction().begin();
		Query query = em.createQuery(
				"SELECT s FROM Student s WHERE s.nrMatricol=:v1").setParameter(
				"v1", nrMatricol);

		toBeUpdatedStudent = (Student) query.getSingleResult();
		String numeGrupa = (String) em
				.createNativeQuery(
						"SELECT grupa.NUME_GRUPA  FROM Grupa,Student WHERE grupa.ID_GRUPA=student.studenti_id_grupa AND student.NR_MATRICOL=?1")
				.setParameter(1, nrMatricol).getSingleResult();
		g = (Grupa) em
				.createQuery("SELECT g FROM Grupa g WHERE g.numeGrupa=:v1")
				.setParameter("v1", numeGrupa).getSingleResult();
		g.getStudenti().remove(g.getStudenti().indexOf(toBeUpdatedStudent));
		em.merge(g);
		toBeUpdatedStudent.setAnStudiu(Integer.valueOf(studentUpdate.getAn())
				.intValue());
		toBeUpdatedStudent.setNrMatricol(studentUpdate.getNrMatricol());
		toBeUpdatedStudent.setNume(studentUpdate.getNumeBaza());
		toBeUpdatedStudent.setPrenume(studentUpdate.getPrenume());
		toBeUpdatedStudent.setEmail(studentUpdate.getEmail());

		if (studentUpdate.getGrupa().equals(numeGrupa)) {
			g = (Grupa) em
					.createQuery("SELECT g FROM Grupa g WHERE g.numeGrupa=:v1")
					.setParameter("v1", numeGrupa).getSingleResult();
			g.getStudenti().add(toBeUpdatedStudent);
			em.merge(g);
		} else {

			g = (Grupa) em
					.createQuery("SELECT g FROM Grupa g WHERE g.numeGrupa=:v1")
					.setParameter("v1", studentUpdate.getGrupa())
					.getSingleResult();

			g.getStudenti().add(toBeUpdatedStudent);
			em.merge(g);
		}
		em.getTransaction().commit();
		em.close();

	}

}
