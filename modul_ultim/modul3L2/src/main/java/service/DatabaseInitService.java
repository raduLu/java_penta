package service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import nom.Catalog;
import nom.Grupa;
import nom.News;
import nom.Obiect;
import nom.Orar;
import nom.Profesor;
import nom.Student;



public class DatabaseInitService {
	private static EntityManagerFactory emf = Persistence
			.createEntityManagerFactory("modul3L2");

	public static void initDatbase() {
		int anStudiuTest = 2;
		String emailTest = "email";
		String numeGrupaTest = "GrupaTest";
		String numeTest = "numeStudentTest";
		String prenumeTest = "prenumeStudentTest";
		int notaTest = 8;
		int nrPrezenteTest = 20;
		int idObiectTest = 5;
		String intervalOrarTest = "12-14";
		int salaCursTest = 3;
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Grupa g = new Grupa();
		g.setNumeGrupa(numeGrupaTest);
		em.persist(g);
		int idGr = g.getIdGrupa();
		final int n = 10;
		for (int i = 0; i < n; i++) {
			Student s = new Student();
			s.setIdGrupa(idGr);
			s.setAnStudiu(anStudiuTest);
			s.setEmail(emailTest);
			s.setNrMatricol(String.valueOf(i));
			s.setNume(numeTest + String.valueOf(i));
			s.setPrenume(prenumeTest + String.valueOf(i));
			Catalog c = new Catalog();
			c.setIdStudent(s.getIdStudent());
			c.setDataNotei(new Date());
			c.setIdObiect(idObiectTest + i);
			c.setNota(notaTest);
			c.setNrPrezente(nrPrezenteTest);
			s.getCatalog().add(c);
			Orar o = new Orar();
			o.setData(new Date());
			o.setIdObiect(idObiectTest + i);
			o.setIntervalOrar(intervalOrarTest);
			o.setSalaCurs(salaCursTest);
			g.getOrar().add(o);
			g.getStudenti().add(s);
		}
		em.persist(g);
		for (int i = 0; i < 5; i++) {
			News nw = new News();
			
			nw.setTitlu("noutati"+i);
			nw.setDataAparitiei(new Date());
			nw.setContinut("Lorem ipsum dolor sit amet, "
					+ "consectetur adipiscing elit. Aenean blandit tortor"
					+ " a ipsum vehicula, in semper sapien auctor. "
					+ "Nulla tempor eget est non consequat. "
					+ "Nulla sit amet lorem justo. "
					+ "Cras non tellus eros.  ");
			em.persist(nw);
		}
		String numeProfesor="numeProfesorTest";
		String prenumeProfesor="prenumeProfesorTest";
		
		for(int i=0;i<5;i++){
			Profesor p=new Profesor();
			p.setEmail("email");
			p.setNume(numeProfesor+i);
			p.setPrenume(prenumeProfesor+i);
			p.setSitePersonal("site");
			
			for(int j=0;j<2;j++){
				Obiect o=new Obiect();
				o.setNume("obiect"+j);
				o.setIdEvaluare(j);
				o.setCredite(j+1);
				p.getObiecte().add(o);
				
			}
			
			em.persist(p);
		}
		em.getTransaction().commit();

	}
}
