package test;




public class NewsBean {
	private String continutNews;
	private String continutShortNews;
	
	
	public String getContinutNews() {
		return continutNews;
	}


	public void setContinutNews(String continutNews) {
		this.continutNews = continutNews;
	}


	public String getContinutShortNews() {
		return continutShortNews;
	}


	public void setContinutShortNews(String continutShortNews) {
		this.continutShortNews = continutShortNews;
	}


	
}
