package test;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.tabview.TabView;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.data.SortEvent;

import nom.Catalog;
import service.DatabaseInitService;
import service.UserService;

@ManagedBean(eager = true)
public class UserLogin {

	// private static final long serialVersionUID = 1L;
	public enum ZILE {
		Luni, Marti, Miercuri, Joi, Vineri
	};

	private String nrMatricol;
	private String email;

	private List<StudentForm> s;
	private List<NewsBean> continutNews;
	private List<ProfesorForm> profesori;
	private List<Catalog> catalog = new ArrayList<Catalog>();
	private List<String> grupe;
	private boolean showCatalog = true;
	private List<CatalogBean> c1 = new ArrayList<CatalogBean>();

	@ManagedProperty(value = "#{actionLinkClass}")
	private ActionLinkClass service;

	@PostConstruct
	public void init() {
		profesori = UserService.creareListaProfesori();
		// s = UserService.creareListaStudentiForm(service.getGrupaAleasa());
		grupe = UserService.creareListaGrupe();
		setContinutNews(UserService.creareListaNews());

	}

	public void updateCatalog() {
		service.setC2(new ArrayList<CatalogBean>());
		service.setCatalogAction("update");
		for (CatalogBean ci : c1) {
			service.getCatalog().get(service.getCatalog().indexOf(ci))
					.setSelected(true);

			service.getC2().add(ci);
			System.out.println(ci.getC().getNrPrezente() + ci.getId()
					+ service.getC2().size());
		}

		showCatalog = false;
	}

	public void aplliedAction() {
		if (service.getCatalogAction().equals("update")) {
			for (CatalogBean ci : service.getC2()) {
				service.getCatalog().get(service.getCatalog().indexOf(ci))
						.setSelected(false);

			}
		}
		if (service.getCatalogAction().equals("remove")) {
			for (CatalogBean ci : service.getC2()) {
				
				service.setCatalog(UserService.aplliedAction(service.getCatalogAction(),service.getC2(),service.getObiectAles()));
			}
		}
		if (service.getCatalogAction().equals("add")) {
			service.getCatalog().get(service.getCatalog().size() - 1)
					.setSelected(false);
		}
		
	}

	public void addCatalog() {
		service.setCatalogAction("add");
		CatalogBean catalogNou = new CatalogBean();
		catalogNou.setSelected(true);
		catalogNou.setC(new Catalog());
		catalogNou.setId(String.valueOf(service.getCatalog().size()));
		catalogNou.getC().setIdObiect(service.getCatalog().get(0).getC().getIdObiect());
		catalogNou.getC().setIdStudent(service.getCatalog().get(0).getC().getIdStudent());
		service.getCatalog().add(catalogNou);
		showCatalog = false;
	}

	public void removeCatalog() {
		service.setC2(new ArrayList<CatalogBean>());
		service.setCatalogAction("remove");
		for (CatalogBean ci : c1) {
			

			service.getC2().add(ci);
		}
	}

	public void listenerTabView(SelectEvent event) {
		Date date1 = (Date) event.getObject();
		service.setZilele(new ArrayList<String>());
		service.setZilele(UserService.generareListaDate(date1));
		SimpleDateFormat formatDate = new SimpleDateFormat("EEEE dd/MM/YYYY");
		service.setTabViewIndex(String.valueOf(service.getZilele().indexOf(
				formatDate.format(date1))));
		service.setOrar(UserService.listareOrar(service.getGrupaAleasa(),
				formatDate.format(service.getDataAleasa())));
		// System.out.println(service.getDataAleasa());
		service.setDataAleasa(new Date());

	}

	public void showOrar() {

		service.setDataAleasa(new Date());

		service.setZilele(new ArrayList<String>());
		service.setZilele(UserService.generareListaDate(service.getDataAleasa()));
		SimpleDateFormat formatDate = new SimpleDateFormat("EEEE dd/MM/YYYY");
		service.setTabViewIndex(String.valueOf(service.getZilele().indexOf(
				formatDate.format(service.getDataAleasa()))));
		service.setOrar(UserService.listareOrar(service.getGrupaAleasa(),
				formatDate.format(service.getDataAleasa())));

	}

	public void onTabChange(TabChangeEvent event) {

		// System.out.println(event.getTab().getTitle()+"celalalt");
		// System.out.println(service.getTabViewIndex()+"asta1");
		if (service.getTabViewIndex() != null) {
			service.setOrar(UserService.listareOrar(
					service.getGrupaAleasa(),
					service.getZilele().get(
							Integer.valueOf(service.getTabViewIndex())
									.intValue())));
			// System.out.println(service.getTabViewIndex()+"asta"+service.getZilele().get(Integer.valueOf(service.getTabViewIndex()).intValue()));
		}

	}

	public void findStudent(int row) {

		// service.sortData();
		service.setStudent(new StudentForm());
		service.setStudent(service.getS().get(row));

		service.setNumarMatricol(service.getS().get(row).getNrMatricol());
		service.removeMessage("student", service.getStudent().getNume(),
				service.getStudent().getNrMatricol());
		// System.out.println(service.getMessage());

	}

	public String removeStudent() {

		UserService.removeStudent(service.getNumarMatricol());
		service.setS(UserService.creareListaStudentiForm(service
				.getGrupaAleasa()));
		return null;
	}

	public String updateStudent() {
		UserService.updateStudent(service.getStudent(),
				service.getNumarMatricol());
		service.setS(UserService.creareListaStudentiForm(service
				.getGrupaAleasa()));
		return null;
	}

	public void populate() {
		DatabaseInitService.initDatbase();
	}

	public String pageControl() {

		return UserService.validUser(getNrMatricol(), getEmail());
		// return "Studentpage?faces-redirect=true";
	}

	public Comparator<ProfesorForm> ProfesorNameComparatorAscending = new Comparator<ProfesorForm>() {

		@Override
		public int compare(ProfesorForm p1, ProfesorForm p2) {
			return p1.getNumeForm().compareTo(p2.getNumeForm());
		}
	};
	public Comparator<ProfesorForm> ProfesorNameComparatorDescending = new Comparator<ProfesorForm>() {

		@Override
		public int compare(ProfesorForm p1, ProfesorForm p2) {
			return p2.getNumeForm().compareTo(p1.getNumeForm());
		}
	};

	public void onSort(SortEvent event) {
		service.setProfesori(UserService.creareListaProfesori());
		if (event.isAscending()) {
			Collections.sort(service.getProfesori(),
					ProfesorNameComparatorAscending);
		} else {
			Collections.sort(service.getProfesori(),
					ProfesorNameComparatorDescending);
		}

	}

	public void updateProfesor(int row) {
		// System.out.println(service.getProfesori().get(row).getNumeForm() +
		// " "
		// + row);
	}

	public String getNrMatricol() {
		return nrMatricol;
	}

	public void setNrMatricol(String nrMatricol) {
		this.nrMatricol = nrMatricol;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<StudentForm> getS() {
		// if (FacesContext.getCurrentInstance().getRenderResponse()) {
		// init();
		// }
		return s;
	}

	public void setS(List<StudentForm> s) {
		// init();
		this.s = s;
	}

	public List<NewsBean> getContinutNews() {
		return continutNews;
	}

	public void setContinutNews(List<NewsBean> continutNews) {
		this.continutNews = continutNews;
	}

	public List<Catalog> getCatalog() {
		return catalog;
	}

	public void setCatalog(List<Catalog> catalog) {
		this.catalog = catalog;
	}

	public ActionLinkClass getService() {
		return service;
	}

	public void setService(ActionLinkClass service) {
		this.service = service;
	}

	public List<ProfesorForm> getProfesori() {
		return profesori;
	}

	public void setProfesori(List<ProfesorForm> profesori) {
		this.profesori = profesori;
	}

	public List<String> getGrupe() {
		return grupe;
	}

	public void setGrupe(List<String> grupe) {
		this.grupe = grupe;
	}

	public boolean isShowCatalog() {
		return showCatalog;
	}

	public void setShowCatalog(boolean showCatalog) {
		this.showCatalog = showCatalog;
	}

	public List<CatalogBean> getC1() {
		return c1;
	}

	public void setC1(List<CatalogBean> c1) {
		this.c1 = c1;
	}

}
