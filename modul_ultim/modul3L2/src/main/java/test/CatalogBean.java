package test;

import nom.Catalog;

public class CatalogBean {
	private String id;
	private Catalog c;
	private boolean selected;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	public Catalog getC() {
		return c;
	}
	public void setC(Catalog c) {
		this.c = c;
	}

}
