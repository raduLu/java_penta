package test;

import javax.faces.bean.ManagedBean;

import org.primefaces.context.RequestContext;

import service.UserService;

@ManagedBean
public class ProfesorForm {

	private String numeProfesor;
	private String prenumeProfesor;
	private String email;
	private String site;
	private String numeForm;
	
	public void reset(){
		this.numeProfesor=null;
		this.email=null;
		this.prenumeProfesor=null;
		this.site=null;
	}
	
	public String add(){
		String result=UserService.adaugaProfesor(this);
		this.reset();
		return null;
	}
	public String getNumeProfesor() {
		return numeProfesor;
	}
	public void setNumeProfesor(String numeProfesor) {
		this.numeProfesor = numeProfesor;
	}
	public String getPrenumeProfesor() {
		return prenumeProfesor;
	}
	public void setPrenumeProfesor(String prenumeProfesor) {
		this.prenumeProfesor = prenumeProfesor;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSite() {
		return site;
	}
	public void setSite(String site) {
		this.site = site;
	}
	public String getNumeForm() {
		return numeForm;
	}
	public void setNumeForm(String numeForm) {
		this.numeForm = numeForm;
	}
	
	
}
