package test;

import javax.faces.bean.ManagedBean;

import service.DatabaseInitService;

@ManagedBean
public class StudentLogin {
	
	private String email;
	private String nrMatricol;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNrMatricol() {
		return nrMatricol;
	}
	public void setNrMatricol(String nrMatricol) {
		this.nrMatricol = nrMatricol;
	}
	
	public String pageControl(){
		DatabaseInitService.initDatbase();
		return "result";
	}

}
