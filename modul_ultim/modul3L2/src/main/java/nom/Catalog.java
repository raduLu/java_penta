package nom;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

@Entity
@EntityListeners(value = { EntityListener.class })
@IdClass(value=CatalogId.class)
public class Catalog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_STUDENT")
    private int idStudent;

    @Id
    @Temporal(TemporalType.DATE)
    @Column(name = "DATA_NOTEI")
    private Date dataNotei;

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dataNotei == null) ? 0 : dataNotei.hashCode());
		result = prime * result + idObiect;
		result = prime * result + idStudent;
		result = prime * result + nota;
		result = prime * result + nrPrezente;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Catalog other = (Catalog) obj;
		if (dataNotei == null) {
			if (other.dataNotei != null)
				return false;
		} else if (!dataNotei.equals(other.dataNotei))
			return false;
		if (idObiect != other.idObiect)
			return false;
		if (idStudent != other.idStudent)
			return false;
		if (nota != other.nota)
			return false;
		if (nrPrezente != other.nrPrezente)
			return false;
		return true;
	}

	@Id
    @Column(name = "ID_OBIECT")
    private int idObiect;

    private int nota;

    @Column(name = "NR_PREZENTE")
    private int nrPrezente;

    public Catalog() {
    }

    public int getIdStudent() {
        return this.idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public Date getDataNotei() {
        return this.dataNotei;
    }

    public void setDataNotei(Date dataNotei) {
        this.dataNotei = dataNotei;
    }

    public int getIdObiect() {
        return this.idObiect;
    }

    public void setIdObiect(int idObiect) {
        this.idObiect = idObiect;
    }

    public int getNota() {
        return this.nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public int getNrPrezente() {
        return this.nrPrezente;
    }

    public void setNrPrezente(int nrPrezente) {
        this.nrPrezente = nrPrezente;
    }

    

}