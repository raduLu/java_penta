package nom;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;

@Entity
@EntityListeners(value = { EntityListener.class })
public class Obiect implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_OBIECT")
    private int idObiect;

    private int credite;

    @Column(name = "ID_EVALUARE")
    private int idEvaluare;

    @Column(name = "ID_PROFESOR")
    private int idProfesor;

    private String nume;

    @ElementCollection(fetch = EAGER)
    private List<String> prezenta;

    @OneToMany(cascade = ALL, fetch = EAGER)
    @JoinColumn
    private List<Orar> orar;

    @ManyToMany(cascade = ALL)
    @JoinTable(name = "evaluare_obiect")
    private Collection<Evaluare> evaluare;

    @OneToMany(fetch = EAGER, cascade = { ALL, MERGE })
    @JoinColumn
    private List<Catalog> catalog;

    @ManyToOne(cascade = { ALL, PERSIST })
    @JoinColumn(name = "prof_ID_PROFESOR", referencedColumnName = "ID_PROFESOR")
    private Profesor prof;

    public Obiect() {
    }

    public int getIdObiect() {
        return this.idObiect;
    }

    public void setIdObiect(int idObiect) {
        this.idObiect = idObiect;
    }

    public int getCredite() {
        return this.credite;
    }

    public void setCredite(int credite) {
        this.credite = credite;
    }

    public int getIdEvaluare() {
        return this.idEvaluare;
    }

    public void setIdEvaluare(int idEvaluare) {
        this.idEvaluare = idEvaluare;
    }

    public int getIdProfesor() {
        return this.idProfesor;
    }

    public void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }

    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public List<String> getPrezenta() {
        return this.prezenta;
    }

    public void setPrezenta(List<String> prezenta) {
        this.prezenta = prezenta;
    }

    public List<Orar> getOrar() {
        return orar;
    }

    public void setOrar(List<Orar> orar) {
        this.orar = orar;
    }

    public Collection<Evaluare> getEvaluare() {
        return evaluare;
    }

    public void setEvaluare(Collection<Evaluare> evaluare) {
        this.evaluare = evaluare;
    }

    public Profesor getProf() {
        return prof;
    }

    public void setProf(Profesor prof) {
        this.prof = prof;
    }

    public List<Catalog> getCatalog() {
        return catalog;
    }

    public void setCatalog(List<Catalog> catalog) {
        this.catalog = catalog;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((catalog == null) ? 0 : catalog.hashCode());
		result = prime * result + credite;
		result = prime * result
				+ ((evaluare == null) ? 0 : evaluare.hashCode());
		result = prime * result + idEvaluare;
		result = prime * result + idObiect;
		result = prime * result + idProfesor;
		result = prime * result + ((nume == null) ? 0 : nume.hashCode());
		result = prime * result + ((orar == null) ? 0 : orar.hashCode());
		result = prime * result
				+ ((prezenta == null) ? 0 : prezenta.hashCode());
		result = prime * result + ((prof == null) ? 0 : prof.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Obiect other = (Obiect) obj;
		if (catalog == null) {
			if (other.catalog != null)
				return false;
		} else if (!catalog.equals(other.catalog))
			return false;
		if (credite != other.credite)
			return false;
		if (evaluare == null) {
			if (other.evaluare != null)
				return false;
		} else if (!evaluare.equals(other.evaluare))
			return false;
		if (idEvaluare != other.idEvaluare)
			return false;
		if (idObiect != other.idObiect)
			return false;
		if (idProfesor != other.idProfesor)
			return false;
		if (nume == null) {
			if (other.nume != null)
				return false;
		} else if (!nume.equals(other.nume))
			return false;
		if (orar == null) {
			if (other.orar != null)
				return false;
		} else if (!orar.equals(other.orar))
			return false;
		if (prezenta == null) {
			if (other.prezenta != null)
				return false;
		} else if (!prezenta.equals(other.prezenta))
			return false;
		if (prof == null) {
			if (other.prof != null)
				return false;
		} else if (!prof.equals(other.prof))
			return false;
		return true;
	}

}