package nom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.PERSIST;

@Entity
@EntityListeners(value = { EntityListener.class })
public class Grupa implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_GRUPA")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idGrupa;

    @Column(name = "NUME_GRUPA")
    private String numeGrupa;

    @OneToMany(cascade = { ALL, PERSIST }, fetch = EAGER)
    @JoinColumn
    private List<Student> studenti = new ArrayList<Student>();

    @OneToMany(fetch = EAGER, cascade = ALL)
    @JoinColumn
    private List<Orar> orar = new ArrayList<Orar>();

    public Grupa() {
    }

    public int getIdGrupa() {
        return this.idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }

    public String getNumeGrupa() {
        return this.numeGrupa;
    }

    public void setNumeGrupa(String numeGrupa) {
        this.numeGrupa = numeGrupa;
    }

    public List<Orar> getOrar() {
        return orar;
    }

    public void setOrar(List<Orar> orar) {
        this.orar = orar;
    }

    public List<Student> getStudenti() {
        return studenti;
    }

    public void setStudenti(List<Student> studenti) {
        this.studenti = studenti;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idGrupa;
		result = prime * result
				+ ((numeGrupa == null) ? 0 : numeGrupa.hashCode());
		result = prime * result + ((orar == null) ? 0 : orar.hashCode());
		result = prime * result
				+ ((studenti == null) ? 0 : studenti.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grupa other = (Grupa) obj;
		if (idGrupa != other.idGrupa)
			return false;
		if (numeGrupa == null) {
			if (other.numeGrupa != null)
				return false;
		} else if (!numeGrupa.equals(other.numeGrupa))
			return false;
		if (orar == null) {
			if (other.orar != null)
				return false;
		} else if (!orar.equals(other.orar))
			return false;
		if (studenti == null) {
			if (other.studenti != null)
				return false;
		} else if (!studenti.equals(other.studenti))
			return false;
		return true;
	}

    

}