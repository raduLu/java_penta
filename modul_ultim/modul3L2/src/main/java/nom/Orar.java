package nom;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

@Entity
@EntityListeners(value = { EntityListener.class })
public class Orar implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_OBIECT")
    private int idObiect;

    @Temporal(TemporalType.DATE)
    private Date data;

    @Column(name = "ID_GRUPA")
    private int idGrupa;

    @Column(name = "INTERVAL_ORAR")
    private String intervalOrar;

    @Column(name = "SALA_CURS")
    private int salaCurs;

    public Orar() {
    }

    public int getIdObiect() {
        return this.idObiect;
    }

    public void setIdObiect(int idObiect) {
        this.idObiect = idObiect;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getIdGrupa() {
        return this.idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }

    public String getIntervalOrar() {
        return this.intervalOrar;
    }

    public void setIntervalOrar(String intervalOrar) {
        this.intervalOrar = intervalOrar;
    }

    public int getSalaCurs() {
        return this.salaCurs;
    }

    public void setSalaCurs(int salaCurs) {
        this.salaCurs = salaCurs;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + idGrupa;
		result = prime * result + idObiect;
		result = prime * result
				+ ((intervalOrar == null) ? 0 : intervalOrar.hashCode());
		result = prime * result + salaCurs;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Orar other = (Orar) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (idGrupa != other.idGrupa)
			return false;
		if (idObiect != other.idObiect)
			return false;
		if (intervalOrar == null) {
			if (other.intervalOrar != null)
				return false;
		} else if (!intervalOrar.equals(other.intervalOrar))
			return false;
		if (salaCurs != other.salaCurs)
			return false;
		return true;
	}

}