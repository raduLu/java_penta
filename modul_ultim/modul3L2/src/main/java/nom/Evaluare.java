package nom;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.*;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.PERSIST;

@Entity
@EntityListeners(value = { EntityListener.class })
public class Evaluare implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_EVALUARE")
    private int idEvaluare;

    @Column(name = "TIP_EVALUARE")
    private String tipEvaluare;

    @ManyToMany(cascade = { ALL, PERSIST })
    private Collection<Obiect> obiect;

    public Evaluare() {
    }

    public String getTipEvaluare() {
        return this.tipEvaluare;
    }

    public void setTipEvaluare(String tipEvaluare) {
        this.tipEvaluare = tipEvaluare;
    }

    public Collection<Obiect> getObiect() {
        return obiect;
    }

    public void setObiect(Collection<Obiect> obiect) {
        this.obiect = obiect;
    }

    public int getIdEvaluare() {
        return idEvaluare;
    }

    public void setIdEvaluare(int idEvaluare) {
        this.idEvaluare = idEvaluare;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idEvaluare;
		result = prime * result + ((obiect == null) ? 0 : obiect.hashCode());
		result = prime * result
				+ ((tipEvaluare == null) ? 0 : tipEvaluare.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Evaluare other = (Evaluare) obj;
		if (idEvaluare != other.idEvaluare)
			return false;
		if (obiect == null) {
			if (other.obiect != null)
				return false;
		} else if (!obiect.equals(other.obiect))
			return false;
		if (tipEvaluare == null) {
			if (other.tipEvaluare != null)
				return false;
		} else if (!tipEvaluare.equals(other.tipEvaluare))
			return false;
		return true;
	}

}