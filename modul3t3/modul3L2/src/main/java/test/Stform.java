package test;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Stform implements Serializable {

	
	private static final long serialVersionUID = 1L;
	  private final int OPT1 = 1;
	   private final int OPT2 = 2;
	   private final int OPT3 = 3;
	   private final int OPTo = 0;
	   private String con;
	  
	   
	   private int selectedOption;
	   private int selectedOption1;
	  
	   public Stform() {
		   selectedOption = OPTo;
		   selectedOption1=0;
	   }

	   public int getSelectedOption() {
	      return selectedOption;
	   }

	   public void setSelectedOption(int selectedOption) {
		   if(selectedOption==3)
		   {
			   selectedOption1=0;
		   }
	      this.selectedOption = selectedOption;
	      
	   }

	   public int getOPT1() {
	      return OPT1;
	   }

	   public int getOPT2() {
	      return OPT2;
	   }

	public int getOPT3() {
		return OPT3;
	}
	

	public int getOPTo() {
		return OPTo;
	}

	
	public int getSelectedOption1() {
		return selectedOption1;
	}

	public void setSelectedOption1(int selectedOption1) {
		this.selectedOption1 = selectedOption1;
	}
	public String getCon() {
		return con;
	}

	public void setCon(String con) {
		this.con = con;
	}

	public void ps(){
		
		setSelectedOption1(1);
	}
	
}
