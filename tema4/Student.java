

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class Student {
	long id;
	public String nume;
	public String prenume;
	int varsta;
	int anDeStudiu;
	String facultate;
	List<List<String>> orar;
	SortedMap<Integer, List<String>> carnet;
	private Scanner consola;

	public Student() {
		consola = new Scanner(System.in);
		System.out.print("Introduceti ID_Student:");
		this.id = consola.nextLong();
		System.out.println("Introduceti nume_Student :");
		this.nume = consola.next();
		System.out.println("Introduceti prenume_Student:");
		this.prenume = consola.next();
		System.out.println("Introduceti varsta_Student:");
		this.varsta = consola.nextInt();
		System.out.println("Introduceti an de studiu_Student:");
		this.anDeStudiu = consola.nextInt();
		System.out.println("Introduceti facultatea_Student:");
		this.facultate = consola.next();

		// creare orar

		orar = new ArrayList<List<String>>();
		List<String> luni = new ArrayList<String>();
		List<String> marti = new ArrayList<String>();
		List<String> miercuri = new ArrayList<String>();
		List<String> joi = new ArrayList<String>();
		List<String> vineri = new ArrayList<String>();

		System.out.print("CreareOrar - Introduceti Ziua(Ex: Joi)sau exit:");
		String ziua = consola.next();

		while (!"exit".equals(ziua)) {

			System.out.print("Introduceti materiile zilei de " + ziua
					+ " si la sfarsit exit");
			String materie = consola.next();
			while (!("exit".equals(materie))) {
				if ("Luni".equals(ziua)) {
					luni.add(materie);
				}
				if ("Marti".equals(ziua)) {
					marti.add(materie);
				}
				if ("Miercuri".equals(ziua)) {
					miercuri.add(materie);
				}
				if ("Joi".equals(ziua)) {
					joi.add(materie);
				}
				if ("Vineri".equals(ziua)) {
					vineri.add(materie);
				}
				materie = consola.next();
			}
			System.out.print("CreareOrar - Introduceti Ziua(Ex: Joi):");
			ziua = consola.next();
		}
		orar.add(luni);
		orar.add(marti);
		orar.add(miercuri);
		orar.add(joi);
		orar.add(vineri);

		// creare carnet
		carnet = new TreeMap<Integer, List<String>>();
		List<String> zece = new ArrayList<String>();
		List<String> noua = new ArrayList<String>();
		List<String> opt = new ArrayList<String>();
		List<String> sapte = new ArrayList<String>();
		List<String> sase = new ArrayList<String>();
		List<String> cinci = new ArrayList<String>();
		List<String> respins = new ArrayList<String>();
		System.out
				.print("CreareCarnet - Introduceti Nota (5-10 sau failed)sau exit:");
		String nota = consola.next();
		while (!nota.equals("exit")) {

			System.out.print("Introduceti materiile pentru nota " + nota
					+ " si la sfarsit exit");
			String materie = consola.next();
			while (!("exit".equals(materie))) {
				if ("10".equals(nota)) {
					zece.add(materie);
				}
				if ("9".equals(nota)) {
					noua.add(materie);
				}
				if ("8".equals(nota)) {
					opt.add(materie);
				}
				if ("7".equals(nota)) {
					sapte.add(materie);
				}
				if ("6".equals(nota)) {
					sase.add(materie);
				}
				if ("5".equals(nota)) {
					cinci.add(materie);
				}
				if ("failed".equals(nota)) {
					respins.add(materie);
				}
				materie = consola.next();
			}
			System.out
					.print("CreareCarnet - Introduceti Nota (5-10 sau failed)sau exit:");
			nota = consola.next();

		}
		if (!(zece.isEmpty()))
			carnet.put((Integer) 10, zece);
		if (!(noua.isEmpty()))
			carnet.put((Integer) 9, noua);
		if (!(opt.isEmpty()))
			carnet.put((Integer) 8, opt);
		if (!(sapte.isEmpty()))
			carnet.put((Integer) 7, sapte);
		if (!(sase.isEmpty()))
			carnet.put((Integer) 6, sase);
		if (!(cinci.isEmpty()))
			carnet.put((Integer) 5, cinci);
		if (!(respins.isEmpty()))
			carnet.put((Integer) 4, respins);
	}

	public void afisare() {
		if (this != null) {
			System.out.println("Studentul " + this.nume + " " + this.prenume
					+ " are id-ul " + this.id + " varsta " + this.varsta
					+ " este in anul de studiu " + this.anDeStudiu
					+ " la facultatea " + this.facultate + " si are orarul ");
			String[] saptamana = { "Luni:", "Marti:", "Miercuri:", "Joi:",
					"Vineri:" };
			int i = 0;
			Iterator<List<String>> contor = this.orar.iterator();
			while (contor.hasNext()) {
				System.out.println(saptamana[i]);
				System.out.println(contor.next());
				i++;
			}
			System.out.println("Carnetul studentului " + this.nume + " "
					+ this.prenume + " ");
			Iterator<Integer> contor1 = this.carnet.keySet().iterator();
			while (contor1.hasNext()) {
				Integer key = contor1.next();
				if (key.equals((Integer) 4)) {
					System.out.println("failed");
					System.out.println(this.carnet.get(key));
				} else {
					System.out.println(key);
					System.out.println(this.carnet.get(key));
				}

			}
		}
	}

	public void scriereFisier(String fis, List<Student> candidati) {
		Path fisier=Paths.get(fis);
		try {
			
			if(Files.notExists(fisier))	{
			Files.createFile(fisier);}
			BufferedWriter writer = Files.newBufferedWriter(fisier,
					Charset.forName("US-ASCII"));
			Iterator<Student> contorCandidat = candidati.iterator();
			List<Set<String>> materiiCandidati = new ArrayList<Set<String>>();
			List<String> numeCandidati = new ArrayList<String>();
			while (contorCandidat.hasNext()) {
				Student candidat = contorCandidat.next();
				Iterator<List<String>> contorOrar = candidat.orar.iterator();

				Set<String> mat = new HashSet<String>();
				while (contorOrar.hasNext()) {
					mat.addAll(contorOrar.next());
				}
				materiiCandidati.add(mat);
				numeCandidati.add(candidat.nume + " " + candidat.prenume);

			}
			int n = materiiCandidati.size();

			for (int i = 0; i < n; i++) {
				for (int j = i + 1; j < n; j++) {
					Set<String> intersection = new HashSet<String>(
							materiiCandidati.get(i));
					intersection.retainAll(materiiCandidati.get(j));
					if (intersection.size() >= 2) {
						writer.write("Studentii: %n" + numeCandidati.get(i)
								+ " si " + numeCandidati.get(j) + " au "
								+ intersection.size() + " materii comune");

					}
				}
			}
			writer.close();
		} catch (FileAlreadyExistsException x) {
			System.err.format("file named %s" + " already exists%n", fisier);
		} catch (IOException e) {
			System.err.format("%s%n", e);
		}
	}

	@Override
	public boolean equals(Object obj) {
		boolean bt, b1, b2, b3;
		if (this == obj)
			return true;
		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;
		Student s = (Student) obj;
		bt = id == s.id && varsta == s.varsta && anDeStudiu == s.anDeStudiu;
		b1 = (facultate != null && facultate.equals(s.facultate))
				&& (nume != null && nume.equals(s.nume));
		b2 = (prenume != null && prenume.equals(s.prenume))
				&& (orar != null && orar.equals(s.orar));
		b3 = (carnet != null && carnet.equals(s.carnet));
		return (bt && b1 && b2 && b3);
	}

	public int hashCode() {
		int hash = 1;
		hash = 13 * hash + (int) id;
		hash = 13 * hash + varsta;
		hash = 13 * hash + anDeStudiu;
		hash = 13 * hash + (null == facultate ? 0 : facultate.hashCode());
		hash = 13 * hash + (null == nume ? 0 : nume.hashCode());
		hash = 13 * hash + (null == prenume ? 0 : prenume.hashCode());
		hash = 13 * hash + (null == orar ? 0 : orar.hashCode());
		hash = 13 * hash + (null == carnet ? 0 : carnet.hashCode());
		return hash;
	}
}