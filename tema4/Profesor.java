
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class Profesor {
	long id;
	public String nume;
	public String prenume;
	int varsta;
	List<String> materii;
	private Scanner consola;

	public Profesor() {
		String s1;
		consola = new Scanner(System.in);
		System.out.println("Introduceti ID_Profesor:");
		this.id = consola.nextLong();
		System.out.println("Introduceti nume_Profesor :");
		this.nume = consola.next();
		System.out.println("Introduceti prenume_Profesor:");
		this.prenume = consola.next();
		System.out.println("Introduceti varsta_Profesor:");
		this.varsta = consola.nextInt();
		materii = new ArrayList<String>();
		System.out.println("Introduceti materiile_profesor:");
		s1 = consola.next();
		while (!(s1.equals("."))) {
			materii.add(s1);
			s1 = consola.next();

		}
	}

	public Profesor(String k) {
		if (k.equals("Materie")) {
			consola = new Scanner(System.in);
			System.out.println("Introduceti ID_Profesor:");
			this.id = consola.nextLong();
			System.out.println("Introduceti nume_Profesor :");
			this.nume = consola.next();
			System.out.println("Introduceti prenume_Profesor:");
			this.prenume = consola.next();
			System.out.println("Introduceti varsta_Profesor:");
			this.varsta = consola.nextInt();
		} else {
			if (k.equals("Inscriere")) {
				System.out.println("Introduceti nume_Profesor :");
				this.nume = consola.next();
				System.out.println("Introduceti prenume_Profesor:");
				this.prenume = consola.next();
			}
		}
	}

	public void checkData() throws IndexOutOfBoundsException, IOException,
			FileNotFoundException {
		FileInputStream fis = null;
		long lastId = 0;
		try {
			fis = new FileInputStream("data.txt");
			lastId = fis.read();

		} catch (FileNotFoundException e) {

		} catch (IOException e) {

		}
		if (lastId < 0) {
			throw new IndexOutOfBoundsException("ID negativ");
		}
	}

	public void afisare() {
		if (this != null) {
			System.out.println("Profesorul " + this.nume + " " + this.prenume
					+ " are id-ul " + this.id + " varsta " + this.varsta
					+ " si preda materiile ");
			Iterator<String> contor = this.materii.iterator();
			while (contor.hasNext()) {
				System.out.println(contor.next());
			}
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;
		Profesor p = (Profesor) obj;
		return id == p.id && varsta == p.varsta
				&& (nume != null && nume.equals(p.nume))
				&& (prenume != null && prenume.equals(p.prenume))
				&& (materii != null && materii.equals(p.materii));
	}

	public int hashCode() {
		int hash = 1;
		hash = 13 * hash + (int) id;
		hash = 13 * hash + (null == nume ? 0 : nume.hashCode());
		hash = 13 * hash + (null == prenume ? 0 : prenume.hashCode());
		hash = 13 * hash + varsta;
		hash = 13 * hash + (null == materii ? 0 : materii.hashCode());
		return hash;
	}

}
