
import java.util.Scanner;

public class Materie {
	long id;
	public String denumire;
	Profesor profesor;
	private Scanner consola;

	public Materie() {
		consola = new Scanner(System.in);
		System.out.println("Introduceti Id_Materie:");
		this.id = consola.nextLong();
		System.out.println("Introduceti Denumire_Materie :");
		this.denumire = consola.next();
		this.profesor = new Profesor("Materie");

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;
		Materie m = (Materie) obj;
		return id == m.id && (denumire != null && denumire.equals(m.denumire))
				&& (profesor != null && profesor.equals(m.profesor));
	}

	public int hashCode() {
		int hash = 1;
		hash = 13 * hash + (int) id;
		hash = 13 * hash + (null == denumire ? 0 : denumire.hashCode());
		hash = 13 * hash + (null == profesor ? 0 : profesor.hashCode());
		return hash;
	}
}
