package test;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class Stform implements Serializable {

	
	private static final long serialVersionUID = 1L;
	  private final int OPT1 = 1;
	   private final int OPT2 = 2;
	   private final int OPT3 = 3;
	   private final int OPTo = 0;
	   private int con=-1;
	  
	   
	   private int selectedOption;
	   
	   public Stform() {
		   selectedOption = OPTo;
		  
	   }

	   public int getSelectedOption() {
	      return selectedOption;
	   }

	   public void setSelectedOption(int selectedOption) {
		   if(selectedOption==3)
		   {
			   
			   con=-1;
		   }
	      this.selectedOption = selectedOption;
	      
	   }

	   public int getOPT1() {
	      return OPT1;
	   }

	   public int getOPT2() {
	      return OPT2;
	   }

	public int getOPT3() {
		return OPT3;
	}
	

	public int getOPTo() {
		return OPTo;
	}

	
	
	public int getCon() {
		return con;
	}

	public void setCon(int con) {
		this.con = con;
	}

	public void ps(int row){
		if(con==-1){
		con=row;}
		else{
			con=-1;
		}
	}
	
	
}
