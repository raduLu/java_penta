package test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

import nom.Catalog;
import nom.Orar;

import service.UserService;
@ManagedBean(eager=true)

public class UserLogin implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private String nrMatricol;
	private String email;
	private List<StudentForm> s;
	private List<NewsBean> continutNews;
	private List<Orar> orar;
	private List<Catalog> catalog;
	
	
	
	
	
	private String dataItem;
	
	@PostConstruct
	    public void init() {
		 s=UserService.creareListaStudentiForm();
		 setContinutNews(UserService.creareListaNews());
		 
	 }
	public String getNrMatricol() {
		return nrMatricol;
	}
	public void setNrMatricol(String nrMatricol) {
		this.nrMatricol = nrMatricol;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<StudentForm> getS() {
		if (FacesContext.getCurrentInstance().getRenderResponse()) {
			init();
		}
		return s;
	}
	public void setS(List<StudentForm> s) {
		init();
		this.s = s;
	}
	public String getDataItem() {
		return dataItem;
	}
	public void setDataItem(String data) {
		this.dataItem = data;
	}
	public List<NewsBean> getContinutNews() {
		return continutNews;
	}
	public void setContinutNews(List<NewsBean> continutNews) {
		this.continutNews = continutNews;
	}
	
	public List<Orar> getOrar() {
		return orar;
	}
	public void setOrar(List<Orar> orar) {
		this.orar = orar;
	}
	public List<Catalog> getCatalog() {
		return catalog;
	}
	public void setCatalog(List<Catalog> catalog) {
		this.catalog = catalog;
	}
	
	public String pageControl(){
	return UserService.validUser(getNrMatricol(),getEmail());
		//return "Studentpage?faces-redirect=true";
	}

	public 	String showOrar(int row){
		String numeGrupa;
		
		numeGrupa=s.get(row).getGrupa();
		orar=new ArrayList<Orar>();
		orar=UserService.listareOrar("GrupaTest");
		numeGrupa=s.get(row).getGrupa();
		return null;
	
		
		
	}
	
}
