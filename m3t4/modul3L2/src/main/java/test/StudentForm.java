package test;

import javax.faces.bean.ManagedBean;

import org.primefaces.context.RequestContext;

import service.UserService;

@ManagedBean
public class StudentForm {

	
	private String nrMatricol;
	private String nume;
	private String prenume;
	private String an;
	private String orar;
	private String catalog;
	private String grupa;
	private String email;
	
	
	public StudentForm(){
		
	}
	
	public String getNrMatricol() {
		return nrMatricol;
	}
	public void setNrMatricol(String nrMatricol) {
		this.nrMatricol = nrMatricol;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getAn() {
		return an;
	}
	public void setAn(String an) {
		this.an = an;
	}
	public String getOrar() {
		return orar;
	}
	public void setOrar(String orar) {
		this.orar = orar;
	}
	public String getCatalog() {
		return catalog;
	}
	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}
	public String getGrupa() {
		return grupa;
	}
	public void setGrupa(String grupa) {
		this.grupa = grupa;
	}

	public String getPrenume() {
		return prenume;
	}

	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String add(){
		String result=UserService.adaugaStudent(this);
		if(result.equals("student existent")){
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("dlg4.hide();");
		
				}
		return null;
		
		
		
	}
}
