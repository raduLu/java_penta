package nom;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;

@Entity
@EntityListeners(value = { EntityListener.class })
@NamedQueries({
        @NamedQuery(name = "QueryStudenti", query = "SELECT s FROM Student s "
                + "WHERE s.nrMatricol = :num"),
        @NamedQuery(name = "DeleteStudenti", query = "DELETE FROM Student s ") })
public class Student implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_STUDENT")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idStudent;

    @Column(name = "AN_STUDIU")
    private int anStudiu;

    private String email;

    @Column(name = "ID_GRUPA")
    private int idGrupa;

    @Column(name = "NR_MATRICOL")
    private String nrMatricol;

    private String nume;

    private String prenume;

    @OneToMany(fetch = EAGER, cascade = ALL)
    @JoinColumn
    private List<Catalog> catalog=new ArrayList<Catalog>();

    public Student() {
    }

    public int getIdStudent() {
        return this.idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public int getAnStudiu() {
        return this.anStudiu;
    }

    public void setAnStudiu(int anStudiu) {
        this.anStudiu = anStudiu;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdGrupa() {
        return this.idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }

    public String getNrMatricol() {
        return this.nrMatricol;
    }

    public void setNrMatricol(String nrMatricol) {
        
            this.nrMatricol=nrMatricol;
        
    }

    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return this.prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public List<Catalog> getCatalog() {
        return catalog;
    }

    public void setCatalog(List<Catalog> catalog) {
        this.catalog = catalog;
    }

    
   

    

}