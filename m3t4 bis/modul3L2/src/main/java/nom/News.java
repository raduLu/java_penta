package nom;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the news database table.
 * 
 */
@Entity
@EntityListeners(value = { EntityListener.class })
public class News implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="ID_NEWS")
    private int idNews;
    @Lob
    private String continut;

    @Temporal(TemporalType.DATE)
    @Column(name="DATA_APARITIEI")
    private Date dataAparitiei;

    private String titlu;

    public News() {
    }

    public int getIdNews() {
        return this.idNews;
    }

    public void setIdNews(int idNews) {
        this.idNews = idNews;
    }

    public String getContinut() {
        return this.continut;
    }

    public void setContinut(String continut) {
        this.continut = continut;
    }

    public Date getDataAparitiei() {
        return this.dataAparitiei;
    }

    public void setDataAparitiei(Date dataAparitiei) {
        this.dataAparitiei = dataAparitiei;
    }

    public String getTitlu() {
        return this.titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

}