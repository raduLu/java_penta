import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.*;

public class DrinkAndChangeTest {

    private DrinkChoice choice,wrongDrinkChoice,wrongQuantityDrinkChoice;

    private VendingMachineStock stocDrinkChange,stockDrinkLowChange;
    private Map<String, Coin> expectedChange;

    @Before
    public void setUp() {
        DrinkChoice d;
        Coin c;
        Coin c1;
        Coin c2;
        d = new DrinkChoice("Fanta", 3, 0.6);
        choice = new DrinkChoice("Fanta", 2, 0.6);

        wrongDrinkChoice=new DrinkChoice("Sprite",2,0.6);
        wrongQuantityDrinkChoice=new DrinkChoice("Fanta",4,0.6);
        c = new Coin(0.5, 1);
        c1 = new Coin(0.2, 3);
        c2 = new Coin(0.1, 3);

        stocDrinkChange = new VendingMachineStock();
        stocDrinkChange.setDrinkStock("Fanta", d);
        stocDrinkChange.setCoinStock("0.5", c);
        stocDrinkChange.setCoinStock("0.2", c1);
        stocDrinkChange.setCoinStock("0.1", c2);
        stockDrinkLowChange= new VendingMachineStock();
        stockDrinkLowChange.setDrinkStock("Fanta", d);
        stockDrinkLowChange.setCoinStock("0.5", c);
        expectedChange = new HashMap<String, Coin>();
        expectedChange.put("0.1", new Coin(0.1, 1));
        expectedChange.put("0.2", new Coin(0.2, 1));

    }

    @After
    public void tearDown() throws Exception {

        stocDrinkChange = null;

        choice = null;
        wrongDrinkChoice=null;
        wrongQuantityDrinkChoice=null;
        stockDrinkLowChange=null;

    }

    @Test
    public final void testNormalConditionBuy() {

        DrinkAndChange resultDrinkChange = new DrinkAndChange(stocDrinkChange);

        try {
            resultDrinkChange.buy(choice, new Coin(0.5, 1), new Coin(0.5, 1),
                    new Coin(0.5, 1));
            for (String key : expectedChange.keySet()) {
                assertTrue("TestNC change", expectedChange.get(key)
                        .getQuantity() == resultDrinkChange.getChange()
                        .get(key).getQuantity());
            }
            assertTrue("TestNC drink", resultDrinkChange.getDrink()
                    .getDrinkName().equals(choice.getDrinkName()));

        } catch (ExceptieAutomat e) {

            System.out.println(e.getMessage());
        }

    }

    @Test
    public final void testExactMoneyBuy() {

        DrinkAndChange resultExactMoney = new DrinkAndChange(stocDrinkChange);
        try {

            resultExactMoney.buy(choice, new Coin(0.5, 2), new Coin(0.2, 1));
            assertTrue("TestNC no change", resultExactMoney.getChange().keySet().isEmpty());
        } catch (ExceptieAutomat e) {

            System.out.println(e.getMessage());
        }

    }

    @Test
    public final void testNotEnoughMoneyBuy() {

        DrinkAndChange resultNotEnoughMoney = new DrinkAndChange(stocDrinkChange);
        try {

        	resultNotEnoughMoney.buy(choice, new Coin(0.5, 2));
           
        } catch (ExceptieAutomat e) {

            assertEquals("Need more money",e.getMessage());
        }

    }
    @Test
    public final void testNotEnoughDrinkBuy() {

        DrinkAndChange resultNotEnoughDrink = new DrinkAndChange(stocDrinkChange);
        try {

        	resultNotEnoughDrink.buy(wrongQuantityDrinkChoice, new Coin(0.5, 2));
           
        } catch (ExceptieAutomat e) {

            assertEquals("Not enough drink",e.getMessage());
        }

    }
    @Test
    public final void testWrongDrinkBuy() {

        DrinkAndChange resultWrongDrink = new DrinkAndChange(stocDrinkChange);
        try {

        	resultWrongDrink.buy(wrongDrinkChoice, new Coin(0.5, 2));
           
        } catch (ExceptieAutomat e) {

            assertEquals("No such drink",e.getMessage());
        }

    }
    @Test
    public final void testLowChangeBuy() {

        DrinkAndChange resultLowChange = new DrinkAndChange(stockDrinkLowChange);
        try {

        	resultLowChange.buy(choice, new Coin(0.5, 3));
           
        } catch (ExceptieAutomat e) {

            assertEquals("Can't make change",e.getMessage());
        }

    }
}