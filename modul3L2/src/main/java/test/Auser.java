package test;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
@ManagedBean(eager=true)
@SessionScoped
public class Auser implements Serializable{
 
	private String nume;
	private static final long serialVersionUID = 1L;
	public Auser(){}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String page(){
		System.out.println("numele "+this.nume);
		return "result.xhtml";
	}
}
