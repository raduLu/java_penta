package test;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
@ManagedBean(eager=true)

public class User implements Serializable {

	
	private static final long serialVersionUID = 1L;
	private String nume=" ";
	private String prenume=" ";
	private String nrMatricol=" ";
	private String grupa=" ";
	
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public String getNrMatricol() {
		return nrMatricol;
	}
	public void setNrMatricol(String nrMatricol) {
		this.nrMatricol = nrMatricol;
	}
	public String getGrupa() {
		return grupa;
	}
	public void setGrupa(String grupa) {
		this.grupa = grupa;
	}
	public String pageControl(){
		return( "result");
	}

}
