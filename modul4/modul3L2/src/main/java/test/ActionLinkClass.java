package test;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;

import nom.Catalog;
import nom.Orar;
import nom.Profesor;

import org.primefaces.event.data.SortEvent;

import service.UserService;

@ManagedBean
@ViewScoped
public class ActionLinkClass implements Serializable {

	private static final long serialVersionUID = 1L;
	private String sortingField = " ";
	private boolean sortingOrder = true;

	private List<StudentForm> s;
	private List<OrarForm> orar;
	private List<CatalogBean> catalog;
	private List<CatalogBean> upCatalog=new ArrayList<CatalogBean>();
	private List<CatalogBean> c2;
	private StudentForm student;
	private String numarMatricol;
	private String grupaAleasa;
	private List<Profesor> profesori;
	private Profesor prof=new Profesor();
	private String message;
	private Date dataAleasa;
	private List<String> zilele;
	private String tabViewIndex;
	private String catalogDialogTitle;
	private String obiectAles;
	private List<String> obiecte;
	private String catalogAction;
	private ProfesorForm updatedProfesor=new ProfesorForm();
	private List<NewsBean> stiri=new ArrayList<NewsBean>();
	
	@PostConstruct
	public void init() {
		profesori = UserService.creareListaProfesori();
		dataAleasa = new Date();
		stiri=UserService.creareListaN();
	}
	public void reset(){
		prof=new Profesor();
		prof.setIdProfesor(0);
		prof.setEmail(" ");
		prof.setNume(" ");
		prof.setSitePersonal(" ");
		prof.setPrenume(" ");
	}
	public void creareStudenti() {
		s = UserService.creareListaStudentiForm(grupaAleasa);
	}

	public void removeMessage(String entitate, String nume, String calitatea) {
		
		if (entitate.equals("student")) {
			message = "Sunteti sigur ca doriti sa stergeti studentul " + nume
					+ " cu numarul matricol " + calitatea + " ?";
		}
		if (entitate.equals("profesor")) {
			message = "Sunteti sigur ca doriti sa stergeti profesorul " + nume
					+ "  " + calitatea + " ?";
		}
	}

	public Comparator<StudentForm> StudentComparatorAscending = new Comparator<StudentForm>() {

		@Override
		public int compare(StudentForm s1, StudentForm s2) {
			String data1 ="", data2 = "";int d3=0,d4=0;
			if (sortingField.equals("Nume")) {
				data1 = s1.getNume();
				data2 = s2.getNume();
			}
			
			if (sortingField.equals("An de studiu")) {
				d3=s1.getAn();
				d4=s2.getAn();
			}
			if (sortingField.equals("Numar Matricol")) {
				data1 = s1.getNrMatricol();
				data2 = s2.getNrMatricol();
			}
			if(data1==""){
				return d3-d4;
			}
			return data1.compareTo(data2);
		}
	};
	public Comparator<StudentForm> StudentComparatorDescending = new Comparator<StudentForm>() {

		@Override
		public int compare(StudentForm s1, StudentForm s2) {
			String data1 ="", data2 = "";int d3=0,d4=0;
			if (sortingField.equals("Nume")) {
				data1 = s1.getNume();
				data2 = s2.getNume();
			}
			
			if (sortingField.equals("An de studiu")) {
				d3=s1.getAn();
				d4=s2.getAn();
			}
			if (sortingField.equals("Numar Matricol")) {
				data1 = s1.getNrMatricol();
				data2 = s2.getNrMatricol();
			}
			if(data1==""){
				return d3-d4;
			}
			return data2.compareTo(data1);
		}
	};
	

	

	public void findStudent(int row) {
		//sortData();
		student = new StudentForm();
		student =  s.get(row);
		
		numarMatricol =  s.get(row).getNrMatricol();
	}
	public void showCatalog(int row) {
		//sortData();
		String nume = s.get(row).getNumeBaza();
			
		String prenume = s.get(row).getPrenume();
		catalogDialogTitle=nume+""+prenume	;
		setObiecte(UserService.generareListaObiecte(nume, prenume));
		catalog=new ArrayList<CatalogBean>();
	}

	public void creareCatalog() {
		
				 catalog= UserService.listareCatalog(obiectAles);
		
		

	}

	public String getSortingField() {
		return sortingField;
	}

	public void setSortingField(String sortingField) {
		this.sortingField = sortingField;
	}

	public boolean isSortingOrder() {
		return sortingOrder;
	}

	public void setSortingOrder(boolean sortingOrder) {
		this.sortingOrder = sortingOrder;
	}

	public List<StudentForm> getS() {
		return s;
	}

	public void setS(List<StudentForm> s) {
		this.s = s;
	}

	public String getNumarMatricol() {
		return numarMatricol;
	}

	public void setNumarMatricol(String numarMatricol) {
		this.numarMatricol = numarMatricol;
	}

	public void onSortStudent(SortEvent event) {
		
		sortingField = event.getSortColumn().getHeaderText();
		sortingOrder = event.isAscending();
		if (event.isAscending()) {
			Collections.sort(s,
					StudentComparatorAscending);
		} else{
			Collections.sort(s,
					StudentComparatorDescending);
		}
	
	}

	public List<OrarForm> getOrar() {

		return orar;
	}

	public void setOrar(List<OrarForm> orar) {

		this.orar = orar;
	}

	public StudentForm getStudent() {
		return student;
	}

	public void setStudent(StudentForm student) {
		this.student = student;
	}

	public List<CatalogBean> getCatalog() {
		return catalog;
	}

	public void setCatalog(List<CatalogBean> catalog) {
		this.catalog = catalog;
	}

	public String getGrupaAleasa() {
		return grupaAleasa;
	}

	public void setGrupaAleasa(String grupaAleasa) {
		this.grupaAleasa = grupaAleasa;
	}

	public List<Profesor> getProfesori() {
		return profesori;
	}

	public void setProfesori(List<Profesor> profesori) {
		this.profesori = profesori;
	}

	public Profesor getProf() {
		return prof;
	}

	public void setProf(Profesor prof) {
		this.prof = prof;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDataAleasa() {
		return dataAleasa;
	}

	public void setDataAleasa(Date dataAleasa) {
		this.dataAleasa = dataAleasa;
	}

	public List<String> getZilele() {
		return zilele;
	}

	public void setZilele(List<String> zilele) {
		this.zilele = zilele;
	}

	public String getTabViewIndex() {
		return tabViewIndex;
	}

	public void setTabViewIndex(String tabViewIndex) {
		this.tabViewIndex = tabViewIndex;
	}

	public List<CatalogBean> getC2() {
		return c2;
	}

	public void setC2(List<CatalogBean> c2) {
		this.c2 = c2;
	}

	public String getCatalogDialogTitle() {
		return catalogDialogTitle;
	}

	public void setCatalogDialogTitle(String catalogDialogTitle) {
		this.catalogDialogTitle = catalogDialogTitle;
	}

	
	public String getObiectAles() {
		return obiectAles;
	}

	public void setObiectAles(String obiectAles) {
		this.obiectAles = obiectAles;
	}

	public List<String> getObiecte() {
		return obiecte;
	}

	public void setObiecte(List<String> obiecte) {
		this.obiecte = obiecte;
	}

	public String getCatalogAction() {
		return catalogAction;
	}

	public void setCatalogAction(String catalogAction) {
		this.catalogAction = catalogAction;
	}

	public List<CatalogBean> getUpCatalog() {
		return upCatalog;
	}

	public void setUpCatalog(List<CatalogBean> upCatalog) {
		this.upCatalog = upCatalog;
	}

	public ProfesorForm getUpdatedProfesor() {
		return updatedProfesor;
	}

	public void setUpdatedProfesor(ProfesorForm updatedProfesor) {
		this.updatedProfesor = updatedProfesor;
	}
	public List<NewsBean> getStiri() {
		return stiri;
	}
	public void setStiri(List<NewsBean> stiri) {
		this.stiri = stiri;
	}
	
}
