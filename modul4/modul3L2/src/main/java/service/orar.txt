for (Orar oIndex : o) {
			obiectOrar = new OrarForm();
			if (formatDate.format(oIndex.getData()).equals(data)) {
				numeObiect = (String) em
						.createQuery(
								"SELECT o.nume FROM Obiect o WHERE o.idObiect=:v")
						.setParameter("v", oIndex.getIdObiect())
						.getSingleResult();
				obiectOrar.setNumeObiect(numeObiect);
				obiectOrar.setData(formatDate.format(oIndex.getData()));
				obiectOrar.setInterval(oIndex.getIntervalOrar());
				obiectOrar.setSala(String.valueOf(oIndex.getSalaCurs()));
				orar.add(obiectOrar);
			}