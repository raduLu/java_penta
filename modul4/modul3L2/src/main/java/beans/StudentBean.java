package beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.primefaces.event.data.SortEvent;

import nom.Grupa;
import nom.Student;

@ManagedBean
@SessionScoped
@SuppressWarnings("unchecked")
public class StudentBean {
	private Student student = new Student();
	private List<Student> listaStudenti = new ArrayList<Student>();
	private String sortingField;
	public Comparator<Student> StudentComparatorAscending = new Comparator<Student>() {
		@Override
		public int compare(Student s1, Student s2) {
			String data1 ="", data2 = "";int d3=0,d4=0;
			if (sortingField.equals("Nume")) {
				data1 = s1.getNume()+s1.getPrenume();
				data2 = s2.getNume()+s2.getPrenume();
				return data1.compareTo(data2);
			}			
			if (sortingField.equals("An de studiu")) {
				d3=s1.getAnStudiu();
				d4=s2.getAnStudiu();
				return d3-d4;
			}
			if (sortingField.equals("Numar Matricol")) {
				d3 = Integer.valueOf(s1.getNrMatricol()).intValue();
				d4 = Integer.valueOf(s2.getNrMatricol()).intValue();
				return d3-d4;
			}
			
			return 0;
		}
	};

	public Comparator<Student> StudentComparatorDescending = new Comparator<Student>() {
		@Override
		public int compare(Student s1, Student s2) {
			String data1 ="", data2 = "";
			int d3=0,d4=0;
			if (sortingField.equals("Nume")) {
				data1 = s1.getNume()+s1.getPrenume();
				data2 = s2.getNume()+s2.getPrenume();
				return data2.compareTo(data1);
			}
			
			if (sortingField.equals("An de studiu")) {
				d3=s1.getAnStudiu();
				d4=s2.getAnStudiu();
				return d3-d4;
			}
			if (sortingField.equals("Numar Matricol")) {
				d3 = Integer.valueOf(s1.getNrMatricol()).intValue();
				d4 = Integer.valueOf(s2.getNrMatricol()).intValue();
				return d3-d4;
			}
			
			return 0;
		}
	};
	@PostConstruct
	private void init(){
		student = new Student();
		listaStudenti = new ArrayList<Student>();
	}
	public Student findStudent(int row){
		Student studentGasit = new Student();
		studentGasit =listaStudenti.get(row);
		return studentGasit;
	}
	
	public void creareListaStudenti(String numeGrupa){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("modul3L2");
		EntityManager em;
		Query query;
		em = emf.createEntityManager();
		em.getTransaction().begin();
		query=em.createQuery("SELECT g FROM Grupa g WHERE g.numeGrupa=:v1")
				.setParameter("v1", numeGrupa);
		listaStudenti=(List<Student>)query.getResultList();
		
		em.getTransaction().commit();
		em.close();
	}
	public String addStudent(Student addedStudent , String numeGrupa){
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("modul3L2");
		EntityManager em;
		Query query,q1,q2;
		Grupa addedStudentGrupa=new Grupa();
		em = emf.createEntityManager();
		em.getTransaction().begin();
		q1=em.createQuery("SELECT s FROM Student s WHERE s.nume=:v1 and s.prenume=:v2")
				.setParameter("v1",addedStudent.getNume())
				.setParameter("v2",addedStudent.getPrenume());
		q2=em.createQuery("SELECT s FROM Student s WHERE s.nrMatricol=:v1")
				.setParameter("v1",addedStudent.getNrMatricol());
		if(!q1.getResultList().isEmpty()||!q2.getResultList().isEmpty()){
			
			return "Nume si Prenume sau Numar Matricol existente";
		}
		query=em.createQuery("SELECT g FROM Grupa g WHERE g.numeGrupa=:v1")
				.setParameter("v1", numeGrupa);
		addedStudentGrupa=(Grupa) query.getSingleResult();
		if(!addedStudentGrupa.equals(null)){
			addedStudent.setIdGrupa(addedStudentGrupa.getIdGrupa());
			addedStudentGrupa.getStudenti().add(addedStudent);
			em.merge(addedStudentGrupa);
		}
		em.getTransaction().commit();
		creareListaStudenti(numeGrupa);
		return null;
		
	}

	public void onSortStudent(SortEvent event) {
		sortingField = event.getSortColumn().getHeaderText();
		if (event.isAscending()) {
			Collections.sort(listaStudenti,StudentComparatorAscending);
		} else{
			Collections.sort(listaStudenti,	StudentComparatorDescending);
		}
	}
	
	public Student getStudent() {
		return student;
	}
	public void setStudent(Student student) {
		this.student = student;
	}
	public List<Student> getListaStudenti() {
		return listaStudenti;
	}
	public void setListaStudenti(List<Student> listaStudenti) {
		this.listaStudenti = listaStudenti;
	}
}
