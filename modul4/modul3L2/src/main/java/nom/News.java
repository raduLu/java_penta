package nom;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the news database table.
 * 
 */
@Entity
@EntityListeners(value = { EntityListener.class })
public class News implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="ID_NEWS")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idNews;
    @Lob
    private String continut;

    @Temporal(TemporalType.DATE)
    @Column(name="DATA_APARITIEI")
    private Date dataAparitiei;

    private String titlu;

    public News() {
    }

    public int getIdNews() {
        return this.idNews;
    }

    public void setIdNews(int idNews) {
        this.idNews = idNews;
    }

    public String getContinut() {
        return this.continut;
    }

    public void setContinut(String continut) {
        this.continut = continut;
    }

    public Date getDataAparitiei() {
        return this.dataAparitiei;
    }

    public void setDataAparitiei(Date dataAparitiei) {
        this.dataAparitiei = dataAparitiei;
    }

   

	public String getTitlu() {
		return titlu;
	}

	public void setTitlu(String titlu) {
		this.titlu = titlu;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((continut == null) ? 0 : continut.hashCode());
		result = prime * result
				+ ((dataAparitiei == null) ? 0 : dataAparitiei.hashCode());
		result = prime * result + idNews;
		result = prime * result + ((titlu == null) ? 0 : titlu.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (continut == null) {
			if (other.continut != null)
				return false;
		} else if (!continut.equals(other.continut))
			return false;
		if (dataAparitiei == null) {
			if (other.dataAparitiei != null)
				return false;
		} else if (!dataAparitiei.equals(other.dataAparitiei))
			return false;
		if (idNews != other.idNews)
			return false;
		if (titlu == null) {
			if (other.titlu != null)
				return false;
		} else if (!titlu.equals(other.titlu))
			return false;
		return true;
	}

}