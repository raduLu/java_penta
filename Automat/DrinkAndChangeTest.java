import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DrinkAndChangeTest {
        private DrinkChoice d;
        private DrinkChoice choice;
        private DrinkChoice wrongDrinkChoice;
        private DrinkChoice wrongQuantityDrinkChoice;
        
        private Coin c;
        private Coin c1;
        private Coin c2;
        private Coin c3;
        private VendingMachineStock stocDrinkChange;
        private DrinkAndChange result1;
        
    @Before
    public void setUp() throws Exception {
        d=new DrinkChoice("Fanta",3,0.6);
        choice=new DrinkChoice("Fanta",2,0.6);
        wrongDrinkChoice=new DrinkChoice("Sprite",2,0.6);
        wrongQuantityDrinkChoice=new DrinkChoice("Fanta",4,0.6);
        c=new Coin(0.5,1);
        c1=new Coin(0.2,3);
        c2=new Coin(0.1,3);
        c3=new Coin(0.1,5);
        stocDrinkChange=new VendingMachineStock();
        stocDrinkChange.setDrinkStock("Fanta", d);
        stocDrinkChange.setCoinStock("0.5", c);
        stocDrinkChange.setCoinStock("0.2", c1);
        stocDrinkChange.setCoinStock("0.1", c2);
        result1 = new DrinkAndChange(stocDrinkChange);
        
    }

    @After
    public void tearDown() throws Exception {
        result1=null;
        stocDrinkChange=null;
        d=null;
        choice=null;
        wrongDrinkChoice=null;
        wrongQuantityDrinkChoice=null;
        c=null;
        c1=null;
        c2=null;
        
    }

    @Test
    public final void testConstructor() {
        
        assertTrue("Result1 is a DrinkAndChange object",result1.getClass().getName().equals("DrinkAndChange"));
        assertNotNull("DrinkAndChange object not null",result1);
    }
     @Test
    public final void testBuy() {
        //test pentru parametru 1
        assertTrue("choice is a DrinkChoice object", 
                choice.getClass().getName().equals("DrinkChoice"));
        assertNotNull("DrinkChoice object not null",choice);
        //test conditii initiale rezultat
        assertEquals("Drink field is initially null",null,result1.getDrink());
        assertEquals("Change field is initially null",null,result1.getChange());
        //test conditii normale bautura si bani
        
        try {
			assertTrue(result1.buy(choice, c,c1,c2).getDrink().equals(choice));
			assertEquals(null,result1.buy(choice, c,c1,c2).getChange().isEmpty());
		} catch (ExceptieAutomat e) {
		
			assertTrue(e.equals(null));
		}
		
       
     }
     
}
