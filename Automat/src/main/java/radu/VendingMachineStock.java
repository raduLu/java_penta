import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class VendingMachineStock implements VendingMachine<VendingMachineStock> {
    private Map<String, DrinkChoice> drinkStock;
    private Map<String, Coin> coinStock;

    public VendingMachineStock() {
    this.drinkStock = new HashMap<String, DrinkChoice>();
    this.coinStock = new HashMap<String, Coin>();
    }

    public Map<String, DrinkChoice> getDrinks() {
    return this.drinkStock;
    }

    public void setDrinkStock(String b, DrinkChoice c) {
    if (this.drinkStock.containsKey(b)) {
        this.drinkStock.get(b).setQuantity(
            c.getQuantity() + this.drinkStock.get(b).getQuantity());
    } else {
        this.drinkStock.put(b, c);
    }
    }

    public void setCoinStock(String b, Coin c) {
    if (this.coinStock.containsKey(b)) {
        this.coinStock.get(b).setQuantity(
            c.getQuantity() + this.coinStock.get(b).getQuantity());
    } else {
        this.coinStock.put(b, c);
    }
    }

    @Override
    public VendingMachineStock buy(DrinkChoice choice, Coin... input)throws  ExceptieAutomat {

    if (this.drinkStock.containsKey(choice.getDrinkName())) {
        if (this.drinkStock.get(choice.getDrinkName()).getQuantity() >= choice.getQuantity()) {
        DrinkChoice temp = new DrinkChoice(choice.getDrinkName(),-(choice.getQuantity()), choice.getPrice().doubleValue());
        this.setDrinkStock(choice.getDrinkName(), temp);
        for (Coin moneda : input) {
            this.setCoinStock(Double.toString(moneda.getDenCoin().doubleValue()),moneda);
        }

        } else {
        throw new ExceptieAutomat("Not enough drink");
        
        }

    } else {
        
        throw new ExceptieAutomat("No such drink");
    }

    return this;
    }

    public VendingMachineStock makeChange(Map<String, Coin> change) {
    for (String key : change.keySet()) {
        this.getCoinsStock().get(key).setQuantity(this.getCoinsStock().get(key).getQuantity()- change.get(key).getQuantity());
    }
    return this;
    }

    public Map<String, Coin> getCoinsStock() {

    return this.coinStock;
    }

    @Override
    public void display() {
        Logger logger = Logger.getLogger("myLogger");
    int i = 0;
    for (String key : this.drinkStock.keySet()) {
        i++;
        logger.info(i + "  " + key + "  "+ this.drinkStock.get(key).getPrice()+" E");
            
        } 
    }

    }


