import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Logger;

public class DrinkAndChange implements VendingMachine<DrinkAndChange> {
    private DrinkChoice drink;
    private Map<String, Coin> change;
    private VendingMachineStock stock;

    public DrinkAndChange(VendingMachineStock s) {
        this.stock = s;
    }

    public Map<String, Coin> makeChange(BigDecimal suma,
            Map<String, Coin> monedeAutomat) {
        Map<String, Coin> sortedCoins = new TreeMap<String, Coin>(monedeAutomat)
                .descendingMap();
        Map<String, Coin> rest = new HashMap<String, Coin>();
        BigDecimal s = suma;
        
        int nrMonede = 0;
        for (String key : sortedCoins.keySet()) {
            nrMonede = s.divide(sortedCoins.get(key).getDenCoin()).setScale(2, RoundingMode.FLOOR).intValue();
            if (nrMonede <= sortedCoins.get(key).getQuantity()) {
                s = s.subtract(sortedCoins.get(key).getDenCoin().multiply(new BigDecimal(nrMonede).setScale(2,RoundingMode.FLOOR)));
               
                if (nrMonede != 0) {
                    rest.put(key, new Coin(sortedCoins.get(key).getDenCoin().doubleValue(), nrMonede));
                }
            } else {
                s = s.subtract(sortedCoins.get(key).getDenCoin().multiply(new BigDecimal(sortedCoins.get(key).getQuantity()).setScale(2,RoundingMode.FLOOR)));
               
                if (sortedCoins.get(key).getQuantity() != 0) {
                    rest.put(key, sortedCoins.get(key));
                }
            }
        }
        if (s.doubleValue() != 0) {
        	
            return null;
        }

        return rest;
    }

    public DrinkChoice getDrink() {
        return this.drink;
    }

    public Map<String, Coin> getChange() {
        return this.change;
    }

    public VendingMachineStock getStock() {
        return this.stock;
    }

    @Override
    public DrinkAndChange buy(DrinkChoice choice, Coin... input)throws ExceptieAutomat {
        BigDecimal insertedSum = BigDecimal.ZERO;
        this.stock = this.stock.buy(choice, input);
        if (this.stock == null) {
            throw new ExceptieAutomat();
        } else {

            for (Coin moneda : input) {
            	 
            	insertedSum = insertedSum.add(moneda.addCoin());
               
            }
            insertedSum = insertedSum.setScale(2, RoundingMode.FLOOR);
            
            BigDecimal owedSum = BigDecimal.ZERO;
            BigDecimal restSum = BigDecimal.ZERO;
            owedSum = choice.getPrice().multiply(new BigDecimal(choice.getQuantity()));
            owedSum = owedSum.setScale(1, RoundingMode.HALF_UP);
           
            if (owedSum.compareTo(insertedSum) != 1) {
                restSum = insertedSum.subtract(owedSum).setScale(2,RoundingMode.FLOOR);
               
                this.change = this.makeChange(restSum,this.stock.getCoinsStock());
            } else {
                throw new ExceptieAutomat("Need more money");
            }
            if (this.change != null) {
                this.stock = this.stock.makeChange(this.change);
                this.drink = choice;
            } else {
                throw new ExceptieAutomat("Can't make change");
            }
        }

        return this;

    }

    @Override
    public void display() {
        Logger logger = Logger.getLogger("myLogger");
        logger.info("Your drink: " + this.drink.getDrinkName() + "  "
                + this.drink.getQuantity() + " cans");
        logger.info("Your change: ");

        for (String key : this.change.keySet()) {
            logger.info(key + "E " + this.change.get(key).getQuantity() + " coin");
        }

    }

}
