import java.util.logging.Logger;


public final class Automat {

    private Automat(){
        
    }
    

    public static void main(String[] args) {
        final int quantity=10;
        final double coin1=0.1;
        final double coin2=0.2;
        final double coin3=0.5;
        final double coin4=1;
        final double coin5=2;
        final String errMessage="Try another option";
        Logger logger = Logger.getLogger("myLogger");
        VendingMachineStock stocAutomat=new VendingMachineStock();
        DrinkChoice d=new DrinkChoice("Fanta",quantity,coin2);
        DrinkChoice d1=new DrinkChoice("Cola",quantity,coin3);
        DrinkChoice d2=new DrinkChoice("Kinley",quantity,coin2+coin1);
        DrinkChoice d3=new DrinkChoice("Sprite",quantity,coin4);
        DrinkChoice d4=new DrinkChoice("Cico",quantity,coin4+coin3);
        Coin c=new Coin(coin1,quantity);
        Coin c1=new Coin(coin2,quantity);
        Coin c2=new Coin(coin3,0);
        Coin c2e=new Coin(coin3,1);
        Coin c3=new Coin(coin4,quantity);
        Coin c4=new Coin(coin5,quantity);
        stocAutomat.setDrinkStock("Fanta", d);
        stocAutomat.setDrinkStock("Cola", d1);
        stocAutomat.setDrinkStock("Kinley", d2);
        stocAutomat.setDrinkStock("Sprite", d3);
        stocAutomat.setDrinkStock("Cico", d4);
        stocAutomat.setCoinStock("0.1", c);
        stocAutomat.setCoinStock("0.2", c1);
        stocAutomat.setCoinStock("0.5", c2);
        stocAutomat.setCoinStock("0.5", c2e);
        stocAutomat.setCoinStock("1", c3);
        stocAutomat.setCoinStock("2", c4);
        Coin inputCoin1=new Coin(coin2,8);
        Coin inputCoin2=new Coin(coin3,3);
        
        DrinkChoice choice=new DrinkChoice("Fanta",2,coin4);
        DrinkAndChange result = new DrinkAndChange(stocAutomat);
        stocAutomat.display();
        try {
            result.buy(choice, inputCoin1,inputCoin2);
            result.display();
            
        } catch (ExceptieAutomat e) {
            
            logger.info(errMessage);
        }
        
        }
        


}
