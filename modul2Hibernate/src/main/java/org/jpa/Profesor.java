package org.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;



@Entity
public class Profesor implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="ID_PROFESOR")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idProfesor;

    private String email;

    private String nume;

    private String prenume;

    @Column(name="SITE_PERSONAL")
    private String sitePersonal;
    
    @OneToMany(mappedBy = "prof")
      private List<Obiect> obiecte;

    public Profesor() {
    }

    public int getIdProfesor() {
        return this.idProfesor;
    }

    public void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return this.prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getSitePersonal() {
        return this.sitePersonal;
    }

    public void setSitePersonal(String sitePersonal) {
        this.sitePersonal = sitePersonal;
    }

    public List<Obiect> getObiecte() {
        return obiecte;
    }

    public void setObiecte(List<Obiect> obiecte) {
        this.obiecte = obiecte;
    }


}