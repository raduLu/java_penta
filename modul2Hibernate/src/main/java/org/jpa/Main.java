package org.jpa;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jpa.Evaluare;


public final class Main {
    private Main(){}
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("mewp");
        EntityManager em = emf.createEntityManager();

        Evaluare ev = new Evaluare();
        ev.setTipEvaluare("EXAMEN");
       
        em.getTransaction().begin();
        em.persist(ev);
        em.getTransaction().commit();
        em.close();
        emf.close();

    }

} 