package org.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;



@Entity
public class Student implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="ID_STUDENT")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idStudent;

    @Column(name="AN_STUDIU")
    private int anStudiu;

    private String email;

    @Column(name="ID_GRUPA")
    private int idGrupa;

    @Column(name="NR_MATRICOL")
    private int nrMatricol;

    private String nume;

    private String prenume;
    
   
   
    @OneToMany
    @JoinColumn
    private List<Catalog> catalog;

    public Student() {
    }

    public int getIdStudent() {
        return this.idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public int getAnStudiu() {
        return this.anStudiu;
    }

    public void setAnStudiu(int anStudiu) {
        this.anStudiu = anStudiu;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getIdGrupa() {
        return this.idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }

    public int getNrMatricol() {
        return this.nrMatricol;
    }

    public void setNrMatricol(int nrMatricol) {
        this.nrMatricol = nrMatricol;
    }

    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return this.prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public List<Catalog> getCatalog() {
        return catalog;
    }

    public void setCatalog(List<Catalog> catalog) {
        this.catalog = catalog;
    }



    

}