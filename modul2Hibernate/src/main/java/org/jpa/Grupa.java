package org.jpa;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;



@Entity
public class Grupa implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="ID_GRUPA")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int idGrupa;

    @Column(name="NUME_GRUPA")
    private String numeGrupa;
    
    @OneToMany
    @JoinColumn
    private List<Student> studenti=new ArrayList<Student>();
    
    @OneToMany
    @JoinColumn
    private List<Orar> orar;
    
    public Grupa() {
    }

    public int getIdGrupa() {
        return this.idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }

    public String getNumeGrupa() {
        return this.numeGrupa;
    }

    public void setNumeGrupa(String numeGrupa) {
        this.numeGrupa = numeGrupa;
    }


    public List<Orar> getOrar() {
        return orar;
    }

    public void setOrar(List<Orar> orar) {
        this.orar = orar;
    }

    public List<Student> getStudenti() {
        return studenti;
    }

    public void setStudenti(List<Student> studenti) {
        this.studenti = studenti;
    }

}