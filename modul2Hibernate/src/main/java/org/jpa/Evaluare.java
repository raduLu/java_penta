package org.jpa;

import java.io.Serializable;
import java.util.Collection;


import javax.persistence.*;

@Entity
public class Evaluare implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id 
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="ID_EVALUARE")
    private int idEvaluare;

    @Column(name="TIP_EVALUARE")
    private String tipEvaluare;
    

    @ManyToMany
    private Collection <Obiect> obiect;
    public Evaluare() {
    }

   

    public String getTipEvaluare() {
        return this.tipEvaluare;
    }

    public void setTipEvaluare(String tipEvaluare) {
        this.tipEvaluare = tipEvaluare;
    }



    public Collection<Obiect> getObiect() {
        return obiect;
    }



    public void setObiect(Collection<Obiect> obiect) {
        this.obiect = obiect;
    }



    public int getIdEvaluare() {
        return idEvaluare;
    }



    public void setIdEvaluare(int idEvaluare) {
        this.idEvaluare = idEvaluare;
    }

    

}