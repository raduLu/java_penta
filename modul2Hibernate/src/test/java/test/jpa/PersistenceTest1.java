package test.jpa;

import static org.junit.Assert.*;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jpa.Grupa;
import org.jpa.Student;
import org.junit.Before;
import org.junit.Test;

public class PersistenceTest1 {
	private EntityManagerFactory emf;
	private Student s;

	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("mewp");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Grupa g = new Grupa();
		
		g.setNumeGrupa("gr1");
		em.persist(g);
		int idgr = g.getIdGrupa();
		for (int i = 0; i < 3; i++) {
			s = new Student();
			s.setIdGrupa(0);
			s.setAnStudiu(1);
			s.setEmail("email");
			s.setNrMatricol(1);
			s.setNume("Student_" + i);
			s.setPrenume("FacultateS_" + i);
			g.getStudenti().add(s);
			em.persist(s);
			em.persist(g);
		}
		em.getTransaction().commit();
		em.close();

	}

	

	@Test
	public final void testStudentPersistence() {
		 EntityManager em = emf.createEntityManager();
		 Query q = em.createQuery("select g from Grupa g");
		 assertTrue(q.getResultList().size() == 1);
		  q = em.createQuery("select s from Student s");
		 assertTrue(q.getResultList().size() == 3);
		 
		 em.close();
	}

}
