package service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.Query;

import test.NewsBean;
import test.StudentForm;

import nom.Catalog;
import nom.News;
import nom.Orar;
import nom.Student;
import nom.Grupa;

public class UserService {
	private static EntityManager em;

	
	
	
	@SuppressWarnings("unchecked")
	public static List<NewsBean> creareListaNews(){
		
		List<News> n=new ArrayList<News>();
		List<NewsBean> data=new ArrayList<NewsBean>();
		NewsBean c= new NewsBean();
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		 em.getTransaction().begin();
		Query query = em.createQuery("SELECT n FROM News n");
		n=query.getResultList();
		int i=0;
		for(News ind:n){
			c.setContinutNews(ind.getContinut());
			c.setContinutShortNews(ind.getContinut().substring(0, 121)+"...");
			
			data.add(c);
		}
		em.getTransaction().commit();
		em.close();
		return data;
		
	}
	public static List<Orar> listareOrar(String numeGrupa){
		
		List<Orar> o=new ArrayList<Orar>();
		List<Grupa> gr = new ArrayList<Grupa>();

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		 em.getTransaction().begin();
		Query query = em.createQuery("SELECT g FROM Grupa g WHERE g.numeGrupa=:v")
						.setParameter("v", numeGrupa);
		gr = (List<Grupa>) query.getResultList();
		if(!gr.isEmpty()){
		o=gr.get(0).getOrar();}
		
		em.getTransaction().commit();
		em.close();
		return o;
		
	}
	public static List<Catalog> listareCatalog(String nume, String prenume){
		List<Catalog>c= new ArrayList<Catalog>();
		List<Student>s= new ArrayList<Student>();
		
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		 em.getTransaction().begin();
		 Query query = em.createQuery("SELECT s FROM Student s WHERE s.nume=:v1 AND s.prenume=:v2")
				 .setParameter("v1", nume)
				 .setParameter("v2", prenume);
		 s=query.getResultList();
		 System.out.print(s.size());
		 if(!s.isEmpty()){
		 c=s.get(0).getCatalog();}
		 em.getTransaction().commit();
		 em.close();
		 return c;
	}
	@SuppressWarnings("unchecked")
	public static List<StudentForm> creareListaStudentiForm() {
		List<StudentForm> stForm=new ArrayList<StudentForm>();
		List<Grupa> gr = new ArrayList<Grupa>();

		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		 em.getTransaction().begin();
		Query query = em.createQuery("SELECT g FROM Grupa g");
		gr = (List<Grupa>) query.getResultList();
		
		if (!gr.isEmpty()) {
			for (Grupa g : gr) {
				for (Student s : g.getStudenti()) {
					StudentForm sf=new StudentForm();
					sf.setNrMatricol(s.getNrMatricol());
					sf.setAn(String.valueOf(s.getAnStudiu()));
					sf.setNume(s.getNume()+" "+s.getPrenume());
					sf.setNumeBaza(s.getNume());
					sf.setGrupa(g.getNumeGrupa());
					sf.setEmail(s.getEmail());
					sf.setPrenume(s.getPrenume());
					sf.setOrar(g.getNumeGrupa());
					sf.setCatalog("Go to ...");
					stForm.add(sf);
				}
			}
			em.getTransaction().commit();
			em.close();
		}
		return stForm;
	}

	public static String validUser(String nrMatricol, String email) {
		List<Student> st = new ArrayList<Student>();
		System.out.print(nrMatricol);
		try {
			EntityManagerFactory emf = Persistence
					.createEntityManagerFactory("modul3L2");
			em = emf.createEntityManager();
			 em.getTransaction().begin();
			Query query = em
					.createQuery(
							"SELECT s FROM Student s WHERE s.nrMatricol=:v1 AND s.email=:v2")
					.setParameter("v1", nrMatricol)
					.setParameter("v2", email);
			st =  query.getResultList();
			
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			// log the exception
		}

		if (st.size()==1) {

			return "second?faces-redirect=true";
		}

		return "failedlogin?faces-redirect=true";

	}

	public static String adaugaStudent(StudentForm studentForm) {
		List<Student> st = new ArrayList<Student>();
		
		try {
			EntityManagerFactory emf = Persistence
					.createEntityManagerFactory("modul3L2");
			em = emf.createEntityManager();
			 em.getTransaction().begin();
			Query query = em
					.createQuery(
							"SELECT s FROM Student s WHERE s.nume=:v1 AND s.prenume=:v2")
					.setParameter("v1", studentForm.getNume())
					.setParameter("v2", studentForm.getPrenume());
			st =  query.getResultList();
			em.getTransaction().commit();
			em.close();
		} catch (Exception e) {
			// log the exception
		}

		if (st.size()==1) {

			return "student existent";
		}else{
			Student s=new Student();
			s.setAnStudiu(Integer.valueOf(studentForm.getAn()).intValue());
			s.setNrMatricol(studentForm.getNrMatricol());
			s.setNume(studentForm.getNume());
			s.setPrenume(studentForm.getPrenume());
			s.setEmail(studentForm.getEmail());
			List<Grupa> g=new ArrayList<Grupa>();
			EntityManagerFactory emf = Persistence
					.createEntityManagerFactory("modul3L2");
			em = emf.createEntityManager();
			em.getTransaction().begin();
			Query query = em
					.createQuery(
							"SELECT g FROM Grupa g WHERE g.numeGrupa=:v1")
					.setParameter("v1", studentForm.getGrupa());
			g=query.getResultList();
			Grupa gr= new Grupa();
			if (g.size()==1){
				
				gr=g.get(0);
				s.setIdGrupa(gr.getIdGrupa());
				gr.getStudenti().add(s);
				em.merge(gr);
				
			}else{
				gr.setNumeGrupa(studentForm.getGrupa());
				s.setIdGrupa(0);
				gr.getStudenti().add(s);
				em.persist(gr);
			}
			
			
		}
		em.getTransaction().commit(); 
		em.close();
		return "Studentpage?faces-redirect=true";
		
	}
	public static StudentForm findStudent(String nrMatricol){
		List<Student> st = new ArrayList<Student>();
		
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		 em.getTransaction().begin();
		Query query = em
				.createQuery(
						"SELECT s FROM Student s WHERE s.nrMatricol=:v1")
				.setParameter("v1", nrMatricol);
				
		st =  query.getResultList();
		
		StudentForm result=new StudentForm();
		if(!st.isEmpty())
		{
			String numeGrupa = (String) em.createNativeQuery("SELECT grupa.NUME_GRUPA  FROM Grupa,Student WHERE grupa.ID_GRUPA=student.studenti_id_grupa AND student.NR_MATRICOL=?1")
	                .setParameter(1, nrMatricol).getSingleResult();
			System.out.println(numeGrupa);
			result.setAn(String.valueOf(st.get(0).getAnStudiu()));
			result.setEmail(st.get(0).getEmail());
			result.setGrupa(numeGrupa);
			result.setNrMatricol(nrMatricol);
			result.setNume(st.get(0).getNume());
			result.setPrenume(st.get(0).getPrenume());
		}
		em.getTransaction().commit();
		em.close();
		return result;
	}
	public static List<StudentForm>  updateStudent(StudentForm student,String nrMatricol){
		List<Student> st = new ArrayList<Student>();
		 
		Catalog c=new Catalog();
		EntityManagerFactory emf = Persistence
				.createEntityManagerFactory("modul3L2");
		em = emf.createEntityManager();
		 em.getTransaction().begin();
		Query query = em
				.createQuery(
						"SELECT s FROM Student s WHERE s.nrMatricol=:v1")
				.setParameter("v1", nrMatricol);
				
		st=query.getResultList();
		System.out.println(st.size());
		em.remove(st.get(0));
		em.getTransaction().commit();
		em.close();
		List<StudentForm> s= creareListaStudentiForm();
		return s;
	}
}
