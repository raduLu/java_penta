package test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import nom.Catalog;
import nom.Orar;

import org.primefaces.event.data.SortEvent;

import service.UserService;

@ManagedBean
@SessionScoped
public class ActionLinkClass implements Serializable {

	private static final long serialVersionUID = 1L;
	private String sortingField=" ";
	private boolean sortingOrder=true;
	
	private List<StudentForm> s;
	private List<Orar> orar;
	private List<Catalog> catalog;
	private StudentForm student;
	private String numarMatricol;

	@PostConstruct
	public void init() {
		s = UserService.creareListaStudentiForm();

	}
	public void sortData(){
		s = UserService.creareListaStudentiForm();
		Map<String, StudentForm> unsortMap = new HashMap<String, StudentForm>();
		if(sortingField.equals("Nume")){
		for(StudentForm stObj:s){
			unsortMap.put(stObj.getNume(), stObj);
		}
		}
		if(sortingField.equals("An de studiu")){
			int i=0;
			for(StudentForm stObj:s){
				unsortMap.put(stObj.getAn()+i, stObj);
				i++;
			}
		}
		if(sortingField.equals("Numar Matricol")){
			for(StudentForm stObj:s){
				unsortMap.put(stObj.getNrMatricol(), stObj);
			}
		}
		Map<String, StudentForm> sortedMap = new TreeMap<String, StudentForm>(unsortMap);
		if(!sortedMap.isEmpty()){
		s=new ArrayList<StudentForm>(sortedMap.values());
		}
		
	}
	public void findStudent(int row){
		
		student=new StudentForm();
		student=sortingOrder?s.get(row):s.get(s.size()-row-1);//s.get(row);
		System.out.print(sortingOrder);
		numarMatricol=sortingOrder?s.get(row).getNrMatricol():s.get(s.size()-row-1).getNrMatricol();//s.get(row).getNrMatricol();
	}
	
	public void showOrar(int row) {
		sortData();
		String numeGrupa=sortingOrder?s.get(row).getGrupa():s.get(s.size()-row-1).getGrupa();
		
		orar = UserService.listareOrar(numeGrupa);

	}
public void showCatalog(int row) {
	sortData();
		String nume=sortingOrder?s.get(row).getNumeBaza():s.get(s.size()-row-1).getNumeBaza();
		String prenume=sortingOrder?s.get(row).getPrenume():s.get(s.size()-row-1).getPrenume();
		catalog=UserService.listareCatalog(nume,prenume);
		System.out.println(nume + prenume);

	}
   public String updateStudent() {
	   
		UserService.updateStudent(student,numarMatricol);
		return null;
	}
	
	public String getSortingField() {
	return sortingField;
}
public void setSortingField(String sortingField) {
	this.sortingField = sortingField;
}
public boolean isSortingOrder() {
	return sortingOrder;
}
public void setSortingOrder(boolean sortingOrder) {
	this.sortingOrder = sortingOrder;
}
public List<StudentForm> getS() {
	return s;
}
public void setS(List<StudentForm> s) {
	this.s = s;
}
public String getNumarMatricol() {
	return numarMatricol;
}
public void setNumarMatricol(String numarMatricol) {
	this.numarMatricol = numarMatricol;
}
	public void onSort(SortEvent event) {
		
		sortingField = event.getSortColumn().getHeaderText();
		sortingOrder = event.isAscending();
		
	}

	public List<Orar> getOrar() {

		return orar;
	}

	public void setOrar(List<Orar> orar) {

		this.orar = orar;
	}
	public StudentForm getStudent() {
		return student;
	}
	public void setStudent(StudentForm student) {
		this.student = student;
	}
	public List<Catalog> getCatalog() {
		return catalog;
	}
	public void setCatalog(List<Catalog> catalog) {
		this.catalog = catalog;
	}

}
