package test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.data.SortEvent;

import nom.Catalog;
import nom.Orar;

import service.UserService;

@ManagedBean(eager = true)

public class UserLogin implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nrMatricol;
	private String email;
	
	private List<StudentForm> s;
	private List<NewsBean> continutNews;
	
	private List<Catalog> catalog;
	
	@ManagedProperty(value = "#{studentForm}")
	private StudentForm student;
	
	@PostConstruct
	public void init() {
		s = UserService.creareListaStudentiForm();
		setContinutNews(UserService.creareListaNews());

	}
	public String pageControl() {
		
		return UserService.validUser(getNrMatricol(), getEmail());
		// return "Studentpage?faces-redirect=true";
	}

	
	
	
	
   
  
	public String getNrMatricol() {
		return nrMatricol;
	}

	public void setNrMatricol(String nrMatricol) {
		this.nrMatricol = nrMatricol;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<StudentForm> getS() {
		//if (FacesContext.getCurrentInstance().getRenderResponse()) {
		//	init();
		//}
		return s;
	}

	public void setS(List<StudentForm> s) {
		//init();
		this.s = s;
	}

	public List<NewsBean> getContinutNews() {
		return continutNews;
	}

	public void setContinutNews(List<NewsBean> continutNews) {
		this.continutNews = continutNews;
	}

	
	public List<Catalog> getCatalog() {
		return catalog;
	}

	public void setCatalog(List<Catalog> catalog) {
		this.catalog = catalog;
	}

	public StudentForm getStudent() {
		return student;
	}

	public void setStudent(StudentForm student) {
		this.student = student;
	}
	

	
}
