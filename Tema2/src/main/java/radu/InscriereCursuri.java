
public class InscriereCursuri<K, V> {
	private K key;
	private V value;

	public InscriereCursuri(K key, V value) {
		this.key = key;
		this.value = value;
	}

	public K getKey() {
		return key;
	}

	public V getValue() {
		return value;
	}

	public void afisareInscriere(K key, V value) throws ExceptiaMea, Exception {

		// Profesor p;
		// Materie m;
		if (!this.value.getClass().getName().equals("Materie")) {
			throw new ExceptiaMea("Tipul lui V este Materie");
		}
		if (this.key.getClass().getName().equals("Profesor")
				|| this.key.getClass().getName().equals("Student")) {
			// p=(Profesor)this.getKey();
			// m=(Materie) this.getValue();

			System.out
					.println(this.key.getClass().getName()
							+ "ul "
							+ (String) key.getClass().getField("nume").get(key)
							+ " "
							+ (String) key.getClass().getField("prenume")
									.get(key)
							+ " este inscris la materia "
							+ (String) value.getClass().getField("denumire")
									.get(value));
		} else {
			throw new ExceptiaMea(
					"Tipul lui K trebuie sa fie Profesor sau Student");
		}
	}
}
