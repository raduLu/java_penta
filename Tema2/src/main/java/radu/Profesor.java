import java.io.*;
import java.util.Scanner;

public class Profesor {
	long id;
	public String nume;
	public String prenume;
	int varsta;
	private Scanner consola;

	public void checkData() throws IndexOutOfBoundsException, IOException,
			FileNotFoundException {
		FileInputStream fis = null;
		long lastId = 0;
		try {
			fis = new FileInputStream("data.txt");
			lastId = fis.read();

		} catch (FileNotFoundException e) {

		} catch (IOException e) {

		}
		if (lastId < 0) {
			throw new IndexOutOfBoundsException("ID negativ");
		}
	}

	public Profesor() {
		consola = new Scanner(System.in);
		System.out.println("Introduceti ID_Profesor:");
		this.id = consola.nextLong();
		System.out.println("Introduceti nume_Profesor :");
		this.nume = consola.next();
		System.out.println("Introduceti prenume_Profesor:");
		this.prenume = consola.next();
		System.out.println("Introduceti varsta_Profesor:");
		this.varsta = consola.nextInt();

	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if ((obj == null) || (obj.getClass() != this.getClass()))
			return false;
		Profesor p = (Profesor) obj;
		return id == p.id && varsta == p.varsta
				&& (nume != null && nume.equals(p.nume))
				&& (prenume != null && prenume.equals(p.prenume));
	}

	public int hashCode() {
		int hash = 1;
		hash = 13 * hash + (int) id;
		hash = 13 * hash + (null == nume ? 0 : nume.hashCode());
		hash = 13 * hash + (null == prenume ? 0 : prenume.hashCode());
		hash = 13 * hash + varsta;
		return hash;
	}

}
