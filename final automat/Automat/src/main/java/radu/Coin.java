import java.math.BigDecimal;
import java.math.RoundingMode;


public class Coin {
    private BigDecimal denCoin;
    private int q;
    
    public Coin(double d,int c){
        this.denCoin=new BigDecimal(d).setScale(2, RoundingMode.FLOOR);
        this.q=c;
    }
    
  
    public BigDecimal getDenCoin(){
        return this.denCoin;
    }
    public int getQuantity(){
        return this.q;
    }
    public void setQuantity(int c){
        this.q=c;
    }
    public BigDecimal addCoin(){
        return this.denCoin.multiply(new BigDecimal(this.q));
    }
}
