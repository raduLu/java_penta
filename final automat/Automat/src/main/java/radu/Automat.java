import java.util.Scanner;
import java.util.logging.Logger;


public final class Automat {

    private Automat(){
        
    }
    

    public static void main(String[] args) {
        
        
        double []coins={0.1,0.2,0.5,1,2};
        
        String nume,option="Standard";
        int q;
        Coin []inputCoin = new Coin[5];
        Logger logger = Logger.getLogger("myLogger");
        @SuppressWarnings("resource")
        Scanner consola=new Scanner(System.in);
        VendingMachineStock stocAutomat=new VendingMachineStock();
        
        logger.info("Stock option(Standard or Custom): ");
        option=consola.next();
        stocAutomat=stocAutomat.restock(option);
        logger.info("Customer drink choice data    ");
        logger.info("Drink Name:  ");
        nume=consola.next();
        logger.info("Drink quantity:");
        q=consola.nextInt();
        DrinkChoice choice=new DrinkChoice(nume,q,(null==stocAutomat.getDrinks().get(nume)? 0:stocAutomat.getDrinks().get(nume).getPrice().doubleValue()));
        logger.info("Customer inserted coins data");
        for(int i=0;i<5;i++){
            logger.info("Number of "+coins[i]+" :");
            q=consola.nextInt();
             inputCoin[i]=new Coin(coins[i],q);
        }
        DrinkAndChange result = new DrinkAndChange(stocAutomat);
        
        try {
            result.buy(choice, inputCoin);
            result.display();
            
            
        } catch (ExceptieAutomat e) {
            
            logger.info("the problem is:  "+e);
        }
        
        }
        


}
