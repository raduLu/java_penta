import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;

public class VendingMachineStock implements VendingMachine<VendingMachineStock> {
    private Map<String, DrinkChoice> drinkStock;
    private Map<String, Coin> coinStock;

    public VendingMachineStock() {
        this.drinkStock = new HashMap<String, DrinkChoice>();
        this.coinStock = new HashMap<String, Coin>();
    }

    public Map<String, DrinkChoice> getDrinks() {
        return this.drinkStock;
    }

    public void setDrinkStock(String b, DrinkChoice c) {
        if (this.drinkStock.containsKey(b)) {
            this.drinkStock.get(b).setQuantity(
                    c.getQuantity() + this.drinkStock.get(b).getQuantity());
        } else {
            this.drinkStock.put(b, c);
        }
    }

    public void setCoinStock(String b, Coin c) {
        if (this.coinStock.containsKey(b)) {

            this.coinStock.get(b).setQuantity(
                    c.getQuantity() + this.coinStock.get(b).getQuantity());
        } else {
            this.coinStock.put(b, c);
        }
    }

    @Override
    public VendingMachineStock buy(DrinkChoice choice, Coin... input)
            throws ExceptieAutomat {

        if (this.drinkStock.containsKey(choice.getDrinkName())) {
            if (this.drinkStock.get(choice.getDrinkName()).getQuantity() >= choice.getQuantity()) {
                DrinkChoice temp = new DrinkChoice(choice.getDrinkName(),-(choice.getQuantity()), choice.getPrice().doubleValue());
                this.setDrinkStock(choice.getDrinkName(), temp);
                for (Coin moneda : input) {

                    this.setCoinStock(
                            Double.toString(moneda.getDenCoin().doubleValue()),
                            moneda);

                }

            } else {
                throw new ExceptieAutomat("Not enough drink");

            }

        } else {

            throw new ExceptieAutomat("No such drink");
        }

        return this;
    }

    public VendingMachineStock makeChange(Map<String, Coin> change) {
        for (String key : change.keySet()) {
            this.getCoinsStock().get(key).setQuantity(this.getCoinsStock().get(key).getQuantity()- change.get(key).getQuantity());
        }
        return this;
    }

    public Map<String, Coin> getCoinsStock() {

        return this.coinStock;
    }

    public VendingMachineStock restock(String option) {
        int quantity = 10;

        double[] coins = { 0.1, 0.2, 0.5, 1.0, 2.0 };
        String nume = "";
        int q;
        double price;
        VendingMachineStock stocAutomat = new VendingMachineStock();
        Logger logger = Logger.getLogger("myLogger");
        @SuppressWarnings("resource")
		Scanner consola = new Scanner(System.in);
        if ("Standard".equals(option)) {

            DrinkChoice d = new DrinkChoice("Fanta", quantity, coins[2]);
            DrinkChoice d1 = new DrinkChoice("Cola", quantity, coins[3]);
            DrinkChoice d2 = new DrinkChoice("Kinley", quantity, coins[2]
                    + coins[1]);
            DrinkChoice d3 = new DrinkChoice("Sprite", quantity, coins[4]);
            DrinkChoice d4 = new DrinkChoice("Cico", quantity, coins[4]
                    + coins[3]);
            Coin c = new Coin(coins[1], quantity);
            Coin c1 = new Coin(coins[2], quantity);
            Coin c2 = new Coin(coins[3], quantity);

            Coin c3 = new Coin(coins[4], quantity);
            Coin c4 = new Coin(coins[5], quantity);
            stocAutomat.setDrinkStock("Fanta", d);
            stocAutomat.setDrinkStock("Cola", d1);
            stocAutomat.setDrinkStock("Kinley", d2);
            stocAutomat.setDrinkStock("Sprite", d3);
            stocAutomat.setDrinkStock("Cico", d4);
            stocAutomat.setCoinStock("0.1", c);
            stocAutomat.setCoinStock("0.2", c1);
            stocAutomat.setCoinStock("0.5", c2);
            stocAutomat.setCoinStock("1.0", c3);
            stocAutomat.setCoinStock("2.0", c4);
            stocAutomat.display();
        } else {
            if ("Custom".equals(option)) {
                logger.info("Insert products data (or exit):");
                logger.info("Drink Name:  ");
                while (!"exit".equals(nume)) {
                    nume = consola.next();
                    logger.info("Drink quantity:");
                    q = consola.nextInt();
                    logger.info("Drink price:");
                    price = consola.nextDouble();
                    DrinkChoice d = new DrinkChoice(nume, q, price);
                    stocAutomat.setDrinkStock(nume, d);
                    logger.info("Drink Name:  ");
                }
                logger.info("Insert coin data:");
                for (int i = 0; i < 5; i++) {
                    logger.info("Number of " + coins[i] + "coins :");
                    q = consola.nextInt();
                    Coin c = new Coin(coins[i], q);
                    stocAutomat.setCoinStock(Double.toString(coins[i]), c);
                }
            }
        }
        return stocAutomat;

    }

    @Override
    public void display() {
        Logger logger = Logger.getLogger("myLogger");
        int i = 0;
        for (String key : this.drinkStock.keySet()) {
            i++;
            logger.info(i + "  " + key + "  "
                    + this.drinkStock.get(key).getPrice() + " E");

        }
        for (String key : this.coinStock.keySet()) {
            i++;
            logger.info("  " + key + "  "
                    + this.coinStock.get(key).getQuantity()
                    + this.coinStock.get(key).getDenCoin());

        }
    }

}
