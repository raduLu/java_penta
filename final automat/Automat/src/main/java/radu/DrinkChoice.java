import java.math.BigDecimal;
import java.math.RoundingMode;


public class DrinkChoice {
    
    private String drinkName;
    private int quantity;
    private BigDecimal price;
    
    public DrinkChoice(String n,int c,double p){
        this.drinkName=n;
        this.quantity=c;
        this.price=new BigDecimal(p).setScale(2, RoundingMode.FLOOR);
    }
    public String getDrinkName(){
        return this.drinkName;
    }
    public int getQuantity(){
        return this.quantity;
    }
    public BigDecimal getPrice(){
        return this.price;
    }
    public void setQuantity(int c){
        this.quantity=c;
    }
    public void setPrice(BigDecimal p){
        this.price=p;
    }

}
