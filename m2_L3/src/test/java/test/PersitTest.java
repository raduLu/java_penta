package test;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jpa.Grupa;
import org.jpa.Student;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class PersitTest {
    private EntityManagerFactory emf;
    private int idGr;
    @Before
    public void setUp() throws Exception {
        emf = Persistence.createEntityManagerFactory("m2_L3");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Grupa g = new Grupa();
        
        g.setNumeGrupa("gr1");
        em.persist(g);
        idGr = g.getIdGrupa();
        final int n=3;
        final int temp=1;
        for (int i = 0; i < n; i++) {
            Student s = new Student();
            s.setIdGrupa(0);
            s.setAnStudiu(temp);
            s.setEmail("email");
            s.setNrMatricol(String.valueOf(temp));
            s.setNume("Student_" + i);
            s.setPrenume("FacultateS_" + i);
            g.getStudenti().add(s);
          
            em.persist(g);
        }
        
       
        em.getTransaction().commit();
                em.close();
                

    }

   @After
    public void tearDown() throws Exception {
        emf=Persistence.createEntityManagerFactory("m2_L3");
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        em.createQuery("DELETE FROM Student st").executeUpdate();
        em.createQuery("DELETE FROM Grupa gr").executeUpdate();
        em.getTransaction().commit();
        em.close();
        
    }


    @Test
    public final void testPersistence() {
        final int temp1=1;
        final int temp2=3;
         EntityManager em = emf.createEntityManager();
         em.getTransaction().begin();
         Query q = em.createQuery("select g from Grupa g");
         assertTrue(q.getResultList().size() == temp1);
          q = em.createQuery("select s from Student s");
         assertTrue(q.getResultList().size() == temp2);
        
         Grupa g = new Grupa();
         
         g=em.find(Grupa.class,idGr);
            List<Student>studenti=new ArrayList<Student>();
            studenti=g.getStudenti();
             assertFalse(studenti.isEmpty());
            em.getTransaction().commit();
            em.close();
                    
    }
    
    @SuppressWarnings("unchecked")
    @Test
    public final void testNamedQuerys(){
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
         Query query = em.createNamedQuery("QueryStudenti");
         query.setParameter("num","Student_1");
         assertFalse(query.getSingleResult()==null);
         
         query=em.createNativeQuery("SELECT * FROM grupa",Grupa.class);
         List<Grupa> gt =(List<Grupa>)query.getResultList();
         assertEquals("gr1",gt.get(0).getNumeGrupa());
         query=em.createNamedQuery("DeleteStudenti");
         int deletedData=query.executeUpdate();
         assertEquals(3,deletedData);
       
        
         
         em.getTransaction().commit();
        
         em.close();
    }

}