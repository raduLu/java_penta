
    create table Catalog (
        ID_STUDENT integer not null,
        DATA_NOTEI date,
        ID_OBIECT integer,
        nota integer not null,
        NR_PREZENTE integer,
        catalog_ID_OBIECT integer,
        catalog_ID_STUDENT integer,
        primary key (ID_STUDENT)
    );

    create table Evaluare (
        ID_EVALUARE integer not null auto_increment,
        TIP_EVALUARE varchar(255),
        primary key (ID_EVALUARE)
    );

    create table Evaluare_Obiect (
        Evaluare_ID_EVALUARE integer not null,
        obiect_ID_OBIECT integer not null
    );

    create table Grupa (
        ID_GRUPA integer not null auto_increment,
        NUME_GRUPA varchar(255),
        primary key (ID_GRUPA)
    );

    create table News (
        ID_NEWS integer not null,
        continut longtext,
        DATA_APARITIEI date,
        titlu varchar(255),
        primary key (ID_NEWS)
    );

    create table Obiect (
        ID_OBIECT integer not null auto_increment,
        credite integer not null,
        ID_EVALUARE integer,
        ID_PROFESOR integer,
        nume varchar(255),
        prof_ID_PROFESOR integer,
        primary key (ID_OBIECT)
    );

    create table Obiect_Evaluare (
        Obiect_ID_OBIECT integer not null,
        evaluare_ID_EVALUARE integer not null
    );

    create table Obiect_prezenta (
        Obiect_ID_OBIECT integer not null,
        prezenta varchar(255)
    );

    create table Orar (
        ID_OBIECT integer not null,
        data date,
        ID_GRUPA integer,
        INTERVAL_ORAR varchar(255),
        SALA_CURS integer,
        orar_ID_GRUPA integer,
        orar_ID_OBIECT integer,
        primary key (ID_OBIECT)
    );

    create table Profesor (
        ID_PROFESOR integer not null auto_increment,
        email varchar(255),
        nume varchar(255),
        prenume varchar(255),
        SITE_PERSONAL varchar(255),
        primary key (ID_PROFESOR)
    );

    create table Student (
        ID_STUDENT integer not null auto_increment,
        AN_STUDIU integer,
        email varchar(255),
        ID_GRUPA integer,
        NR_MATRICOL varchar(255),
        nume varchar(255),
        prenume varchar(255),
        studenti_ID_GRUPA integer,
        primary key (ID_STUDENT)
    );

    alter table Catalog 
        add index FK8457F7F9B0F92B9B (catalog_ID_OBIECT), 
        add constraint FK8457F7F9B0F92B9B 
        foreign key (catalog_ID_OBIECT) 
        references Obiect (ID_OBIECT);

    alter table Catalog 
        add index FK8457F7F9541DF1FB (catalog_ID_STUDENT), 
        add constraint FK8457F7F9541DF1FB 
        foreign key (catalog_ID_STUDENT) 
        references Student (ID_STUDENT);

    alter table Evaluare_Obiect 
        add index FKBF74974440776DA2 (obiect_ID_OBIECT), 
        add constraint FKBF74974440776DA2 
        foreign key (obiect_ID_OBIECT) 
        references Obiect (ID_OBIECT);

    alter table Evaluare_Obiect 
        add index FKBF74974410011513 (Evaluare_ID_EVALUARE), 
        add constraint FKBF74974410011513 
        foreign key (Evaluare_ID_EVALUARE) 
        references Evaluare (ID_EVALUARE);

    alter table Obiect 
        add index FK8C651B00AB89122F (prof_ID_PROFESOR), 
        add constraint FK8C651B00AB89122F 
        foreign key (prof_ID_PROFESOR) 
        references Profesor (ID_PROFESOR);

    alter table Obiect_Evaluare 
        add index FK670D86DA40776DA2 (Obiect_ID_OBIECT), 
        add constraint FK670D86DA40776DA2 
        foreign key (Obiect_ID_OBIECT) 
        references Obiect (ID_OBIECT);

    alter table Obiect_Evaluare 
        add index FK670D86DA10011513 (evaluare_ID_EVALUARE), 
        add constraint FK670D86DA10011513 
        foreign key (evaluare_ID_EVALUARE) 
        references Evaluare (ID_EVALUARE);

    alter table Obiect_prezenta 
        add index FKDD8B9EC40776DA2 (Obiect_ID_OBIECT), 
        add constraint FKDD8B9EC40776DA2 
        foreign key (Obiect_ID_OBIECT) 
        references Obiect (ID_OBIECT);

    alter table Orar 
        add index FK25A1743AE6EB16 (orar_ID_OBIECT), 
        add constraint FK25A1743AE6EB16 
        foreign key (orar_ID_OBIECT) 
        references Obiect (ID_OBIECT);

    alter table Orar 
        add index FK25A174D7C988A0 (orar_ID_GRUPA), 
        add constraint FK25A174D7C988A0 
        foreign key (orar_ID_GRUPA) 
        references Grupa (ID_GRUPA);

    alter table Student 
        add index FKF3371A1B9C9A91A6 (studenti_ID_GRUPA), 
        add constraint FKF3371A1B9C9A91A6 
        foreign key (studenti_ID_GRUPA) 
        references Grupa (ID_GRUPA);
