package org.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

@Entity
@EntityListeners(value = { EntityListener.class })
public class Catalog implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_STUDENT")
    private int idStudent;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATA_NOTEI")
    private Date dataNotei;

    @Column(name = "ID_OBIECT")
    private int idObiect;

    private int nota;

    @Column(name = "NR_PREZENTE")
    private int nrPrezente;

    public Catalog() {
    }

    public int getIdStudent() {
        return this.idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public Date getDataNotei() {
        return this.dataNotei;
    }

    public void setDataNotei(Date dataNotei) {
        this.dataNotei = dataNotei;
    }

    public int getIdObiect() {
        return this.idObiect;
    }

    public void setIdObiect(int idObiect) {
        this.idObiect = idObiect;
    }

    public int getNota() {
        return this.nota;
    }

    public void setNota(int nota) {
        this.nota = nota;
    }

    public int getNrPrezente() {
        return this.nrPrezente;
    }

    public void setNrPrezente(int nrPrezente) {
        this.nrPrezente = nrPrezente;
    }

}