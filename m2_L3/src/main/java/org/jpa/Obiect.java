package org.jpa;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import javax.persistence.*;

import org.jpa.Evaluare;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;

@Entity
@EntityListeners(value = { EntityListener.class })
public class Obiect implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_OBIECT")
    private int idObiect;

    private int credite;

    @Column(name = "ID_EVALUARE")
    private int idEvaluare;

    @Column(name = "ID_PROFESOR")
    private int idProfesor;

    private String nume;

    @ElementCollection(fetch = EAGER)
    private List<String> prezenta;

    @OneToMany(cascade = ALL, fetch = EAGER)
    @JoinColumn
    private List<Orar> orar;

    @ManyToMany
    private Collection<Evaluare> evaluare;

    @OneToMany(fetch = EAGER, cascade = { ALL, MERGE })
    @JoinColumn
    private List<Catalog> catalog;

    @ManyToOne
    private Profesor prof;

    public Obiect() {
    }

    public int getIdObiect() {
        return this.idObiect;
    }

    public void setIdObiect(int idObiect) {
        this.idObiect = idObiect;
    }

    public int getCredite() {
        return this.credite;
    }

    public void setCredite(int credite) {
        this.credite = credite;
    }

    public int getIdEvaluare() {
        return this.idEvaluare;
    }

    public void setIdEvaluare(int idEvaluare) {
        this.idEvaluare = idEvaluare;
    }

    public int getIdProfesor() {
        return this.idProfesor;
    }

    public void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }

    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public List<String> getPrezenta() {
        return this.prezenta;
    }

    public void setPrezenta(List<String> prezenta) {
        this.prezenta = prezenta;
    }

    public List<Orar> getOrar() {
        return orar;
    }

    public void setOrar(List<Orar> orar) {
        this.orar = orar;
    }

    public Collection<Evaluare> getEvaluare() {
        return evaluare;
    }

    public void setEvaluare(Collection<Evaluare> evaluare) {
        this.evaluare = evaluare;
    }

    public Profesor getProf() {
        return prof;
    }

    public void setProf(Profesor prof) {
        this.prof = prof;
    }

    public List<Catalog> getCatalog() {
        return catalog;
    }

    public void setCatalog(List<Catalog> catalog) {
        this.catalog = catalog;
    }

}