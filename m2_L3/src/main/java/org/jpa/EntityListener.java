package org.jpa;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;

import javax.persistence.PostPersist;

public class EntityListener {

    @PostPersist
    public <T> void prePersistEntities(T entitate) {
        Logger log = Logger.getLogger("InfoLogging");
        log.info(entitate.getClass().getSimpleName());

        Method[] metode = entitate.getClass().getDeclaredMethods();
        for (Method m : metode) {

            if (m.getName().contains(
                    "get" + "Id" + entitate.getClass().getSimpleName())) {
                try {
                    log.info(String.valueOf(m.invoke(entitate)));
                } catch (IllegalAccessException | IllegalArgumentException
                        | InvocationTargetException e) {

                    log.info(e + "");
                }
            }
        }

    }

}