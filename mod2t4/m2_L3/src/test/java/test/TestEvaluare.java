package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.jpa.Evaluare;
import org.jpa.Obiect;
import org.junit.After;
import org.junit.Test;

public class TestEvaluare {
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("m2_L3");

    @After
    public void tearDown() throws Exception {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.createQuery("DELETE FROM Obiect o").executeUpdate();
        em.createQuery("DELETE FROM Evaluare ev").executeUpdate();
        em.getTransaction().commit();
        em.close();
    }

    
    @Test
    public void testPersistUpdateDeleteEvaluare() {
        
        String tipEvaluare="EvaluareTest";
        Evaluare ev=new Evaluare();
        Evaluare evT=new Evaluare();
        Evaluare evT1=new Evaluare();
        ev.setTipEvaluare(tipEvaluare);
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(ev);
        em.getTransaction().commit();
        evT = (Evaluare) em.createQuery("select evT from Evaluare evT where evT.tipEvaluare='EvaluareTest' ").getSingleResult();
        assertTrue("Nu a fost introdusa o inregistrare","EvaluareTest".equals(evT.getTipEvaluare()));
        em.getTransaction().begin();
        evT.setTipEvaluare("EvaluareTest1");
        em.getTransaction().commit();
        evT1=(Evaluare) em.createQuery("select ev from Evaluare ev where ev.tipEvaluare='EvaluareTest1' ").getSingleResult();
        assertTrue("Nu a fost facuta schimbarea ","EvaluareTest1".equals(evT1.getTipEvaluare()));
        em.getTransaction().begin();
        int deletedData = em.createQuery("Delete from Evaluare e").executeUpdate();
        em.getTransaction().commit();
        assertEquals("Tabela avea mai mult de o inregistrare",deletedData, 1);
        em.close();

    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Test
    public void testObiectRelation(){
        int credite=5;
        int idProfesor=1;
        String nume="ObiectTest";
        List<String> prezenta=new ArrayList<String>();
        String tipEvaluare="EvaluareTest";
        Obiect o = new Obiect();
        Obiect o1 = new Obiect();
        o.setCredite(credite);
        o.setIdProfesor(idProfesor);
        o.setNume(nume);
        o.setPrezenta(prezenta);
        o1.setCredite(credite-1);
        o1.setNume(nume+"1");
        o1.setPrezenta(prezenta);
        o1.setIdProfesor(idProfesor+1);
        List<Obiect> obev=new ArrayList<Obiect>();
        obev.add(o1);
        obev.add(o);
        Evaluare ev=new Evaluare();
        ev.setTipEvaluare(tipEvaluare);
        ev.setObiect(obev);
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(ev);
        em.getTransaction().commit();   
        List<Obiect> ob=(List<Obiect>)em.createQuery("select o from Obiect o").getResultList();
        assertEquals("Nu au fost persistate obiectele",ob.size(),2);
        List gt = em.createNativeQuery("SELECT *  FROM evaluare_obiect").getResultList();
         Iterator itr = gt.iterator();
         while (itr.hasNext()) {
             Object[] obj = (Object[]) itr.next();
             
             String value2 =String.valueOf(obj[1]);
             Obiect oT=new Obiect();
             oT=em.find(Obiect.class, Integer.valueOf(value2));
             assertTrue("Nu a fost gasit macar un obiect",oT.getNume().equals(nume)||oT.getNume().equals(nume+"1"));
             
         }
        em.close();
    }

}
