package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({PersitTest.class,
			   TestEvaluare.class,
			   TestObiect.class,
			   TestGrupa.class,
			   TestStudent.class})
public class JpaTests {

}
