package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jpa.Obiect;
import org.jpa.Profesor;
import org.junit.After;
import org.junit.Test;

public class TestObiect {
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("m2_L3");

    @After
    public void tearDown() throws Exception {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.createQuery("DELETE FROM Obiect o").executeUpdate();
        em.createQuery("DELETE FROM Evaluare ev").executeUpdate();
        em.getTransaction().commit();
        em.close();
	}

	@Test
	public void testPersistUpdateDeleteObiect() {
		
		int credite=5;
        int idProfesor=1;
        String nume="ObiectTest";
        List<String> prezenta=new ArrayList<String>();
        Obiect o = new Obiect();
        EntityManager em = emf.createEntityManager();
        o.setCredite(credite);
        o.setIdProfesor(idProfesor);
        o.setNume(nume);
        o.setPrezenta(prezenta);
        em.getTransaction().begin();
        em.persist(o);
        em.getTransaction().commit();
        Obiect o1 = new Obiect();
        o1=(Obiect)em.createQuery("Select o from Obiect o").getSingleResult();
        assertEquals("Nu a fost persistat obiectul",o1.getIdObiect(),o.getIdObiect());
        em.getTransaction().begin();
        o1.setNume(nume+"Update");
        
        o=(Obiect)em.createQuery("Select o from Obiect o").getSingleResult();
        assertTrue("Nu a fost updatat obiectul",o.getNume().equals(nume+"Update"));
        
        int deletedData=em.createQuery("delete from Obiect o").executeUpdate();
        assertTrue(deletedData==1);
        em.getTransaction().commit();
        em.close();
		
	}
	@SuppressWarnings("unchecked")
	@Test
	public void testProfesorRelation(){
		
		 int credite=5;
	        int idProfesor=1;
	        String nume="ObiectTest";
	        List<String> prezenta=new ArrayList<String>();
	        
	        String numeProfesor="NumeTest";
	        String prenumeProfesor="PrenumeTest";
	        String email="EmailTest";
	        String site="SiteTest";
	        EntityManager em = emf.createEntityManager();
	        Obiect o = new Obiect();
	        Obiect o1 = new Obiect();
	        Profesor p=new Profesor();
	        Profesor p1=new Profesor();
	        
	        o.setCredite(credite);
	        o.setIdProfesor(idProfesor);
	        o.setNume(nume);
	        o.setPrezenta(prezenta);
	        o1.setCredite(credite-1);
	        o1.setNume(nume+"1");
	        o1.setPrezenta(prezenta);
	        
	        List<Obiect> obev=new ArrayList<Obiect>();
	        obev.add(o1);
	        obev.add(o);
	        p.setEmail(email);
	        p.setNume(numeProfesor);
	        p.setPrenume(prenumeProfesor);
	        p.setSitePersonal(site);
	        p.setObiecte(obev);
	        em.getTransaction().begin();
	        em.persist(p);
	        em.getTransaction().commit();
	        List<Obiect> ob=em.createQuery("Select o from Orar o").getResultList();
	        for(Obiect oind:ob){
	        	assertTrue((oind.getNume().equals(o1.getNume())||oind.getNume().equals(o.getNume()))&&ob.size()==2);
	        }
	        em.getTransaction().begin();
	        em.createQuery("Delete from Profesor p").executeUpdate();
	        em.getTransaction().commit();
	        p1.setEmail(email);
	        p1.setNume(numeProfesor);
	        p1.setPrenume(prenumeProfesor);
	        p1.setSitePersonal(site);
	        o1.setProf(p1);
	        o.setProf(p1);
	        em.getTransaction().begin();
	        em.persist(p1);
	        em.persist(o1);
	        em.persist(o);
	        em.getTransaction().commit();
	        
	        List<String>  obn =(List<String>) em.createNativeQuery("SELECT obiect.nume FROM Obiect,Profesor where obiect.prof_ID_PROFESOR=profesor.ID_PROFESOR")
	        		.getResultList();
	        for(String oind:obn){
	        	assertTrue((oind.equals(o1.getNume())||oind.equals(o.getNume()))&&obn.size()==2);
	        }
	        em.close();
	        
	}

}
