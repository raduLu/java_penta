package test;

import static org.junit.Assert.*;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.jpa.Grupa;
import org.jpa.Orar;
import org.jpa.Student;
import org.junit.After;
import org.junit.Test;

public class TestGrupa {
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("m2_L3");
	


	@After
	public void tearDown() throws Exception {

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.createQuery("DELETE FROM Orar o").executeUpdate();
		em.createQuery("DELETE FROM Student st").executeUpdate();
		em.createQuery("DELETE FROM Grupa gr").executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	@Test
	public void testPersistUpdateDelete() {
		String numeGrupaTest = "GrupaTest";
		String numeGrupaTest1 = "GrupaTest1";
		Grupa g = new Grupa();

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		g.setNumeGrupa(numeGrupaTest);
		em.persist(g);
		em.getTransaction().commit();
		Query q = em.createQuery("select grT from Grupa grT where grT.numeGrupa='GrupaTest' ");
		Grupa grT=new Grupa();
		grT=(Grupa) q.getSingleResult();
		assertTrue("Nu a fost persistat ","GrupaTest".equals(grT.getNumeGrupa()));
		em.getTransaction().begin();
		grT.setNumeGrupa(numeGrupaTest1);
		em.getTransaction().commit();
		q = em.createQuery("select grT from Grupa grT  ");
		Grupa grT1=new Grupa();
		grT1=(Grupa) q.getSingleResult();
		assertEquals("Nu a fost schimbat numele grupei",grT1.getNumeGrupa(), numeGrupaTest1);
		em.getTransaction().begin();
		q = em.createQuery("Delete from Grupa gr");
		int deletedData = q.executeUpdate();
		em.getTransaction().commit();
		assertEquals("Tabela avea mai mult de o inregistrare",deletedData, 1);
		em.close();
	}
	
	public Student initTest1() {
		int anStudiuTest = 2;
		String emailTest = "email";
		String numarMatricolTest = "1";
		String numeTest = "numeStudentTest";
		String prenumeTest = "prenumeStudentTest";
		Student s = new Student();
		s.setIdGrupa(1);
		s.setAnStudiu(anStudiuTest);
		s.setEmail(emailTest);
		s.setNrMatricol(numarMatricolTest);
		s.setNume(numeTest);
		s.setPrenume(prenumeTest);
		return s;

	}
	@Test
	public void testStudentRelation(){
		String numeGrupaTest = "GrupaTest";
		Grupa g = new Grupa();
		Student s = new Student();

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		s=initTest1();
		g.setNumeGrupa(numeGrupaTest);
		g.getStudenti().add(s);
		em.persist(g);
		Query q = em.createQuery("select grT from Grupa grT ");
		Grupa grT1=new Grupa();
		grT1=(Grupa) q.getSingleResult();
		assertTrue("Nu a fost introdus in grupa","GrupaTest".equals(grT1.getNumeGrupa()));
		 q = em.createQuery("select st from Student st where st.nume='numeStudentTest' ");
		Student st = new Student();
		st = (Student) q.getSingleResult();
		assertTrue("Nu a fost introdus in student","numeStudentTest".equals(st.getNume()));
		
		em.getTransaction().commit();
	}
	public Orar initTest2() {
		int idObiectTest=3;
		String intervalOrarTest="12-14";
		int salaCursTest=3;
		Orar o=new Orar();
		o.setData(new Date());
		o.setIdObiect(idObiectTest);
		o.setIntervalOrar(intervalOrarTest);
		o.setSalaCurs(salaCursTest);
		return o;
		
	}
	@Test
	public void testOrarRelation(){
		String numeGrupaTest = "GrupaTest";
		Grupa g = new Grupa();
		Orar o = new Orar();

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		o=initTest2();
		g.setNumeGrupa(numeGrupaTest);
		
		em.persist(g);
	    o.setIdGrupa(g.getIdGrupa());
	    g.getOrar().add(o);
	    em.persist(g);
	    em.getTransaction().commit();
		Query q = em.createQuery("select grT from Grupa grT ");
		Grupa grT1=new Grupa();
		grT1=(Grupa) q.getSingleResult();
		assertTrue("Nu a fost introdus in grupa","GrupaTest".equals(grT1.getNumeGrupa()));
		 q = em.createQuery("select oT from Orar oT where oT.intervalOrar='12-14' ");
		 Orar oT = new Orar();
		 oT=(Orar) q.getSingleResult();
		 assertTrue("Nu a fost introdus in orar","12-14".equals(oT.getIntervalOrar()));
		 em.close();
	}
	
}
