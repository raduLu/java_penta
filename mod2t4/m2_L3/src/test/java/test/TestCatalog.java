package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jpa.Catalog;
import org.jpa.Obiect;
import org.jpa.Student;
import org.junit.After;
import org.junit.Test;

public class TestCatalog {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("m2_L3");

    @After
    public void tearDown() throws Exception {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
        em.createQuery("DELETE FROM Catalog c").executeUpdate();
        em.createQuery("delete from Student s").executeUpdate();
        em.createQuery("delete from Obiect o").executeUpdate();
        em.getTransaction().commit();
        em.close();
	}

	@Test
	public void testPersistUpdateDeleteCatalog() {	
		int idStudentTest=1;
		int idObiectTest=1;
		Date data=new Date();
		int nota=8;
		int nrPrezente=7;
		 EntityManager em = emf.createEntityManager();
		 Catalog c=new Catalog();
		 Catalog cT=new Catalog();
		 c.setDataNotei(data);
		 c.setIdObiect(idObiectTest);
		 c.setIdStudent(idStudentTest);
		 c.setNota(nota);
		 c.setNrPrezente(nrPrezente);
	     em.getTransaction().begin();
	     em.persist(c);
	        
	     em.getTransaction().commit();
	     cT=(Catalog) em.createQuery("Select c from Catalog c where c.idStudent=1").getSingleResult();
	     assertTrue(cT.getIdStudent()==c.getIdStudent());
	     em.getTransaction().begin();
	     cT.setNota(7);
	     em.getTransaction().commit();
	     c=(Catalog) em.createQuery("Select c from Catalog c where c.idStudent=1").getSingleResult();
	     assertTrue(c.getNota()==7);
	     em.getTransaction().begin();
	     int deletedData=em.createQuery("Delete from Catalog c where c.idStudent=1").executeUpdate();
	     em.getTransaction().commit();
	     assertTrue(deletedData==1);
	     em.close();
		
	}
	
	@Test
	public void testStudentRelation(){
		int idStudentTest=1;
		int idObiectTest=1;
		Date data=new Date();
		int nota=8;
		int nrPrezente=7;
		 EntityManager em = emf.createEntityManager();
		 Catalog c=new Catalog();
		
		 Student s=new Student();
		 
		 c.setDataNotei(data);
		 c.setIdObiect(idObiectTest);
		 c.setIdStudent(idStudentTest);
		 c.setNota(nota);
		 c.setNrPrezente(nrPrezente);
		 s.getCatalog().add(c);
	     em.getTransaction().begin();
	     em.persist(s);
	     
	     em.getTransaction().commit();
	     int dataTest=(int) em.createNativeQuery("select catalog.id_student from catalog, student where catalog.catalog_id_student=student.id_student").getSingleResult();
	     assertEquals(dataTest,1);
	     em.close();
		
	}
	
	@Test
	public void testObiectRelation(){
		int idStudentTest=1;
		int idObiectTest=1;
		Date data=new Date();
		int nota=8;
		int nrPrezente=7;
		 EntityManager em = emf.createEntityManager();
		 Catalog c=new Catalog();
		
		 Obiect o=new Obiect();
		 
		 c.setDataNotei(data);
		 c.setIdObiect(idObiectTest);
		 c.setIdStudent(idStudentTest);
		 c.setNota(nota);
		 c.setNrPrezente(nrPrezente);
		 List<Catalog> c_o=new ArrayList<Catalog>();
		 c_o.add(c);
		 o.setCatalog(c_o);
	     em.getTransaction().begin();
	     em.persist(o);
	     em.getTransaction().commit();
	     int dataTest=(int) em.createNativeQuery("select catalog.id_obiect from catalog, obiect where catalog.catalog_id_obiect=obiect.id_obiect").getSingleResult();
	     assertEquals(dataTest,1);
	     em.close();
	}

}
