package test;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.jpa.News;
import org.junit.After;
import org.junit.Test;

public class TestNews {
private EntityManagerFactory emf = Persistence.createEntityManagerFactory("m2_L3");

    @After
    public void tearDown() throws Exception {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        
       
        em.createQuery("delete from News n").executeUpdate();
        em.getTransaction().commit();
        em.close();}

	@SuppressWarnings("unchecked")
	@Test
	public void testPersistUpdateDeleteNews() {
		String titluTest="Titlunewstest";
		String continut="ContinutContinutContinutContinutContinutContinutContinutContinut";
		Date dataTest=new Date();
		News n=new News();
		News nT=new News();
		n.setContinut(continut);
		n.setDataAparitiei(dataTest);
		n.setTitlu(titluTest);
		 EntityManager em = emf.createEntityManager();
	     em.getTransaction().begin();
	     em.persist(n);
	     em.getTransaction().commit();
	     nT=(News) em.createQuery("Select n from News n where n.titlu=:titluTest").setParameter("titluTest", titluTest).getSingleResult();
	     assertTrue(nT.getTitlu()==n.getTitlu());
	     em.getTransaction().begin();
	     nT.setTitlu(titluTest+"Update");
	     em.getTransaction().commit();
	     n=(News) em.createQuery("Select n from News n where n.titlu=:titluTest").setParameter("titluTest", titluTest+"Update").getSingleResult();
	     assertEquals(titluTest+"Update",n.getTitlu());
	     em.getTransaction().begin();
	     nT=em.find(News.class, n.getIdNews());
	     em.remove(nT);
	     
	     em.getTransaction().commit();
	     List<News>nTest=(List<News>) em.createQuery("Select n from News n ")
	    		 .getResultList();
	     assertTrue(nTest.isEmpty());
	     em.close();
	}

}
