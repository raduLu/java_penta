package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.jpa.Catalog;
import org.jpa.Grupa;
import org.jpa.Student;
import org.junit.After;
import org.junit.Test;

public class TestStudent {
	private EntityManagerFactory emf = Persistence
			.createEntityManagerFactory("m2_L3");
	

	@After
	public void tearDown() throws Exception {

		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		em.createQuery("DELETE FROM Catalog c").executeUpdate();
		em.createQuery("DELETE FROM Student st").executeUpdate();
		em.createQuery("DELETE FROM Grupa gr").executeUpdate();
		em.getTransaction().commit();
		em.close();
	}

	public Student initTest1() {
		int anStudiuTest = 2;
		String emailTest = "email";
		String numarMatricolTest = "1";
		String numeTest = "numeStudentTest";
		String prenumeTest = "prenumeStudentTest";
		Student s = new Student();
		s.setIdGrupa(1);
		s.setAnStudiu(anStudiuTest);
		s.setEmail(emailTest);
		s.setNrMatricol(numarMatricolTest);
		s.setNume(numeTest);
		s.setPrenume(prenumeTest);
		return s;

	}

	@Test
	public void testPersistDeleteUpdateStudent() {
		String numeTestUpdate = "numeStudentUpdate";
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Student studentTest = new Student();
		studentTest = initTest1();
		em.persist(studentTest);
		em.getTransaction().commit();
		Query q = em.createQuery("select st from Student st where st.nume='numeStudentTest' ");
		Student st = new Student();
		st = (Student) q.getSingleResult();
		assertTrue("numeStudentTest".equals(st.getNume()));
		em.getTransaction().begin();
		st.setNume(numeTestUpdate);
		em.getTransaction().commit();
		q = em.createQuery("select st from Student st");
		Student st1 = new Student();
		st1 = (Student) q.getSingleResult();
		assertEquals(st1.getNume(), numeTestUpdate);
		em.getTransaction().begin();
		q = em.createQuery("Delete from Student s");
		int deletedData = q.executeUpdate();
		em.getTransaction().commit();
		assertEquals(deletedData, 1);
		em.close();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testPersistThroughGrupaRelation() {
		int anStudiuTest = 2;
		String emailTest = "email";
		String numeGrupaTest = "GrupaTest";
		String numeTest = "numeStudentTest";
		String prenumeTest = "prenumeStudentTest";
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Grupa g = new Grupa();

		g.setNumeGrupa(numeGrupaTest);
		em.persist(g);
		int idGr = g.getIdGrupa();
		final int n = 3;

		for (int i = 0; i < n; i++) {
			Student s = new Student();
			s.setIdGrupa(idGr);
			s.setAnStudiu(anStudiuTest);
			s.setEmail(emailTest);
			s.setNrMatricol(String.valueOf(i));
			s.setNume(numeTest + String.valueOf(i));
			s.setPrenume(prenumeTest + String.valueOf(i));
			g.getStudenti().add(s);
			em.persist(g);
		}
		em.getTransaction().commit();
		em.getTransaction().begin();
		Grupa gTest = new Grupa();
		gTest = em.find(Grupa.class, idGr);
		assertTrue("GrupaTest".equals(gTest.getNumeGrupa()));
		Query q = em.createQuery("select st from Student st ");
		List<Student> st = new ArrayList<Student>();
		st = (List<Student>) q.getResultList();
		assertTrue(st.equals(gTest.getStudenti()));
		em.getTransaction().commit();
		em.close();

	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCatalogRelation() {

		int anStudiuTest = 2;
		String emailTest = "email";
		String numarMatricolTest = "1";
		String numeTest = "numeStudentTest";
		String prenumeTest = "prenumeStudentTest";
		int notaTest = 8;
		int nrPrezenteTest = 20;
		int idObiectTest = 5;
		final int n = 3;
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Student studentTest = new Student();
		studentTest.setIdGrupa(1);
		studentTest.setAnStudiu(anStudiuTest);
		studentTest.setEmail(emailTest);
		studentTest.setNrMatricol(numarMatricolTest);
		studentTest.setNume(numeTest);
		studentTest.setPrenume(prenumeTest);

		em.persist(studentTest);
		for (int i = 0; i < n; i++) {
			Catalog catalogTest = new Catalog();
			catalogTest.setIdStudent(studentTest.getIdStudent());
			catalogTest.setDataNotei(new Date());

			catalogTest.setIdObiect(idObiectTest + i);
			catalogTest.setNota(notaTest);
			catalogTest.setNrPrezente(nrPrezenteTest);
			studentTest.getCatalog().add(catalogTest);
		}

		em.persist(studentTest);
		em.getTransaction().commit();
		em.getTransaction().begin();
		Student st = new Student();
		st = em.find(Student.class, studentTest.getIdStudent());
		Query q = em.createQuery("select ct from Catalog ct ");
		List<Catalog> ct = new ArrayList<Catalog>();
		ct = (List<Catalog>) q.getResultList();
		assertEquals(ct.size(), 3);
		assertTrue(ct.equals(st.getCatalog()));
		em.getTransaction().commit();
		em.close();
	}

	@SuppressWarnings("rawtypes")
	@Test
	public void testChangeGrupa() {
		int anStudiuTest = 2;
		String emailTest = "email";
		String numarMatricolTest = "1";
		String numeTest = "numeStudentTest";
		String numeGrupaTest = "GrupaTest";
		String numeGrupaTest1 = "GrupaTest1";
		String prenumeTest = "prenumeStudentTest";
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		Grupa g = new Grupa();

		g.setNumeGrupa(numeGrupaTest);

		em.persist(g);

		int idGr = g.getIdGrupa();

		Student studentTest = new Student();
		studentTest.setIdGrupa(idGr);
		studentTest.setAnStudiu(anStudiuTest);
		studentTest.setEmail(emailTest);
		studentTest.setNrMatricol(numarMatricolTest);
		studentTest.setNume(numeTest);
		studentTest.setPrenume(prenumeTest);
		g.getStudenti().add(studentTest);
		em.persist(g);
		Query q = em.createNativeQuery("SELECT student.studenti_id_grupa  FROM Student WHERE student.studenti_id_grupa=?1")
				.setParameter(1, idGr);
		List st = q.getResultList();
		q = em.createQuery("select s from Student s");
		Student s = new Student();
		s = (Student) q.getSingleResult();
		assertEquals(st.size(), 1);
		Grupa g1 = new Grupa();
		g1.setNumeGrupa(numeGrupaTest1);
		em.persist(g1);
		int idGr1 = g1.getIdGrupa();
		g1.getStudenti().add(s);
		em.persist(g1);
		q = em.createNativeQuery(
				"SELECT student.studenti_id_grupa  FROM Student WHERE student.studenti_id_grupa=?1")
				.setParameter(1, idGr1);
		List st1 = q.getResultList();
		assertEquals(st1.size(), 1);

		em.getTransaction().commit();
		Iterator itr = st.iterator();
		Integer stIdGr=0;
		while (itr.hasNext()) {
			 stIdGr =  (Integer) itr.next();
		}
		 itr = st1.iterator();
		 Integer stIdGr1=0;
		while (itr.hasNext()) {
			stIdGr1 =  (Integer) itr.next();
			
		}
		assertFalse(stIdGr1.equals(stIdGr));
		em.close();

	}

}
