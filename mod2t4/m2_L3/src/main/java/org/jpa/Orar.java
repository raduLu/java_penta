package org.jpa;

import java.io.Serializable;
import javax.persistence.*;

import java.util.Date;

@Entity
@EntityListeners(value = { EntityListener.class })
public class Orar implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID_OBIECT")
    private int idObiect;

    @Temporal(TemporalType.DATE)
    private Date data;

    @Column(name = "ID_GRUPA")
    private int idGrupa;

    @Column(name = "INTERVAL_ORAR")
    private String intervalOrar;

    @Column(name = "SALA_CURS")
    private int salaCurs;

    public Orar() {
    }

    public int getIdObiect() {
        return this.idObiect;
    }

    public void setIdObiect(int idObiect) {
        this.idObiect = idObiect;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getIdGrupa() {
        return this.idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }

    public String getIntervalOrar() {
        return this.intervalOrar;
    }

    public void setIntervalOrar(String intervalOrar) {
        this.intervalOrar = intervalOrar;
    }

    public int getSalaCurs() {
        return this.salaCurs;
    }

    public void setSalaCurs(int salaCurs) {
        this.salaCurs = salaCurs;
    }

}