package org.jpa;

import java.io.Serializable;
import java.util.Date;

public class CatalogId implements Serializable {
    private static final long serialVersionUID = 1L;
    
        private int idStudent;
    private int idObiect;
    private Date dataNotei;
    
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
            }
        if (obj == null){
            return false;
        }
        if (getClass() != obj.getClass()){
            return false;
        }
        CatalogId other = (CatalogId) obj;
        
        return (idObiect==other.idObiect)&&(idStudent==other.idStudent)&&(dataNotei.equals(other.dataNotei));
    }
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + idObiect;
        result = prime * result + idStudent;
        result = prime * result +dataNotei.hashCode();
        return result;
    }
    public int getIdStudent() {
        return idStudent;
    }
    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }
    public int getIdObiect() {
        return idObiect;
    }
    public void setIdObiect(int idObiect) {
        this.idObiect = idObiect;
    }
    public Date getDataNotei() {
        return dataNotei;
    }
    public void setDataNotei(Date dataNotei) {
        this.dataNotei = dataNotei;
    }

}
