package org.jpa;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public final class Main {
    private static  Logger log = Logger.getLogger("InfoLogging");

    private Main() {
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("m2_L3");
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Grupa g = new Grupa();

        g.setNumeGrupa("gr1");
        em.persist(g);
        final int n = 3;
        final int temp = 1;
        for (int i = 0; i < n; i++) {
            Student s = new Student();
            
            s.setIdGrupa(0);
            s.setAnStudiu(temp);
            s.setEmail("email");
            s.setNrMatricol(String.valueOf(temp));
            s.setNume("Student_" + i);
            s.setPrenume("FacultateS_" + i);
            g.getStudenti().add(s);

            em.persist(g);

        }

        em.getTransaction().commit();
        em.getTransaction().begin();
        Query query = em.createNamedQuery("QueryStudenti");
        query.setParameter("num", "Student_1");
        List gt = em.createNativeQuery("SELECT student.nume,student.prenume  FROM Grupa,Student WHERE grupa.ID_GRUPA=student.studenti_id_grupa")
                .getResultList();
        printNativeQuery(gt);
        List<Student> st = (List<Student>) query.getResultList();
        printStudentQuery(st);
        query = em.createNamedQuery("DeleteStudenti");
        int deletedData = query.executeUpdate();
        printStudentDeletedQuery(deletedData);
        em.getTransaction().commit();
em.close();
emf.close();
        
        
        
    

    }

    public static  void printStudentDeletedQuery(int deletedData) {
        log.info("Stergere reusita....");

        log.info(deletedData + "   sterse");

    }

    public static  void printStudentQuery(List<Student> st) {
        log.info("Informatii din baza de date Studenti....");
        for (Student sind : st) {
            log.info(sind.getNrMatricol() + "    "+sind.getNume());
        }
    }

    @SuppressWarnings("rawtypes")
    public static  void printNativeQuery(List gt) {
         Iterator itr = gt.iterator();
         while (itr.hasNext()) {
             Object[] obj = (Object[]) itr.next();
             String nume = String.valueOf(obj[0]);
             String prenume =String.valueOf(obj[1]);
             log.info(nume + "  " + prenume);
         }

    }
}