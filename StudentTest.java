import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.*;

public class StudentTest {

	@Test
	public final void testScriereFisier() {
		Student s=new Student();
		Student s1=new Student();
		Student s2=new Student();
		List<Student> test=new ArrayList<Student>();
		test.add(s);
		test.add(s1);
		test.add(s2);
		String caleTest = "c:\\juno\\workspace\\Tema1\\fisiertest.txt";
		s.scriereFisier(caleTest, test);
		assertTrue("Nu a fost creat un fisier:",new File(caleTest).exists());
		assertTrue("Nu a fost scris nimic in ",new File(caleTest).length()!=0);
		assertEquals(s, test.get(0));
		assertEquals(s1, test.get(1));
		assertEquals(s2, test.get(2));
	}

}
