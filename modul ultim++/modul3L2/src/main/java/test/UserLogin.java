package test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.data.SortEvent;

import nom.Catalog;
import nom.Profesor;
import service.DatabaseInitService;
import service.UserService;

@ManagedBean(eager = true)
public class UserLogin {

	// private static final long serialVersionUID = 1L;
	

	private String nrMatricol;
	private String email;

	private List<StudentForm> s;
	private List<NewsBean> continutNews;
	private List<Profesor> profesori;
	private List<Catalog> catalog = new ArrayList<Catalog>();
	private List<String> grupe;

	private List<CatalogBean> c1 = new ArrayList<CatalogBean>();

	@ManagedProperty(value = "#{actionLinkClass}")
	private ActionLinkClass service;

	@PostConstruct
	public void init() {
		profesori = UserService.creareListaProfesori();
		
		grupe = UserService.creareListaGrupe();
		setContinutNews(UserService.creareListaNews());

	}
	public void cancel(){
		service.creareCatalog();
	}
	public String updateProfesor(){
		
		UserService.updateProfesor(service.getProf());
		profesori=UserService.creareListaProfesori();
		return null;
	}
	public void addProfesor(){
		UserService.adaugaProfesor(service.getProf());
		service.setProfesori(UserService.creareListaProfesori());
		service.reset();
	}
	public void updateCatalog() {
		for (CatalogBean ci : c1) {
			service.getCatalog().get(service.getCatalog().indexOf(ci))
					.setSelected(true);
		}
		c1 = new ArrayList<CatalogBean>();
	}

	public void aplliedAction() {
		for (CatalogBean ci : service.getCatalog()) {
			ci.setSelected(false);
		
		}
		UserService.aplliedAction(service.getCatalog());
		service.creareCatalog();
	}

	public void addCatalog() {
		service.setCatalogAction("add");
		CatalogBean catalogNou = new CatalogBean();
		catalogNou.setSelected(true);
		catalogNou.setC(new Catalog());
		service.getCatalog().add(catalogNou);
		
	}

	public void removeCatalog() {
		for (CatalogBean ci : c1) {
			service.getCatalog().remove(ci);
			
		}
		c1 = new ArrayList<CatalogBean>();
	}

	public void listenerTabView(SelectEvent event) {
		Date date1 = (Date) event.getObject();
		service.setZilele(new ArrayList<String>());
		service.setZilele(UserService.generareListaDate(date1));
		SimpleDateFormat formatDate = new SimpleDateFormat("EEEE dd/MM/YYYY");
		service.setTabViewIndex(String.valueOf(service.getZilele().indexOf(
				formatDate.format(date1))));
		service.setOrar(UserService.listareOrar(service.getGrupaAleasa(),
				formatDate.format(service.getDataAleasa())));
		
		service.setDataAleasa(new Date());

	}

	public void showOrar() {

		service.setDataAleasa(new Date());

		service.setZilele(new ArrayList<String>());
		service.setZilele(UserService.generareListaDate(service.getDataAleasa()));
		SimpleDateFormat formatDate = new SimpleDateFormat("EEEE dd/MM/YYYY");
		service.setTabViewIndex(String.valueOf(service.getZilele().indexOf(
				formatDate.format(service.getDataAleasa()))));
		service.setOrar(UserService.listareOrar(service.getGrupaAleasa(),
				formatDate.format(service.getDataAleasa())));

	}

	public void onTabChange(TabChangeEvent event) {

		// System.out.println(event.getTab().getTitle()+"celalalt");
		// System.out.println(service.getTabViewIndex()+"asta1");
		if (service.getTabViewIndex() != null) {
			service.setOrar(UserService.listareOrar(
					service.getGrupaAleasa(),
					service.getZilele().get(
							Integer.valueOf(service.getTabViewIndex())
									.intValue())));
			// System.out.println(service.getTabViewIndex()+"asta"+service.getZilele().get(Integer.valueOf(service.getTabViewIndex()).intValue()));
		}

	}

	public void findStudent(int row) {

		// service.sortData();
		service.setStudent(new StudentForm());
		service.setStudent(service.getS().get(row));

		service.setNumarMatricol(service.getS().get(row).getNrMatricol());
		service.removeMessage("student", service.getStudent().getNume(),
				service.getStudent().getNrMatricol());
		// System.out.println(service.getMessage());

	}
	public void findProfesor(int row){
		service.setProf(service.getProfesori().get(row));
		
		service.removeMessage("profesor", service.getProf().getNume(),
				service.getProf().getPrenume());
	}
	public void removeProfesor(){
		UserService.removeProfesor(service.getProf());
		service.setProfesori(UserService.creareListaProfesori());
		
	}

	public String removeStudent() {

		UserService.removeStudent(service.getNumarMatricol());
		service.setS(UserService.creareListaStudentiForm(service
				.getGrupaAleasa()));
		return null;
	}

	public String updateStudent() {
		UserService.updateStudent(service.getStudent(),
				service.getNumarMatricol());
		service.setS(UserService.creareListaStudentiForm(service
				.getGrupaAleasa()));
		return null;
	}

	public void populate() {
		DatabaseInitService.initDatbase();
	}

	public String pageControl() {

		return UserService.validUser(getNrMatricol(), getEmail());
		// return "Studentpage?faces-redirect=true";
	}

	public Comparator<Profesor> ProfesorNameComparatorAscending = new Comparator<Profesor>() {

		@Override
		public int compare(Profesor p1, Profesor p2) {
			return p1.getNume().compareTo(p2.getNume())==0?p1.getPrenume().compareTo(p2.getPrenume()):p1.getNume().compareTo(p2.getNume());
		}
	};
	public Comparator<Profesor> ProfesorNameComparatorDescending = new Comparator<Profesor>() {

		@Override
		public int compare(Profesor p1, Profesor p2) {
			return p2.getNume().compareTo(p1.getNume())==0?p2.getPrenume().compareTo(p1.getPrenume()):p2.getNume().compareTo(p1.getNume());
		}
	};

	public void onSort(SortEvent event) {
		
		if (event.isAscending()) {
			Collections.sort(service.getProfesori(),
					ProfesorNameComparatorAscending);
		} else {
			Collections.sort(service.getProfesori(),
					ProfesorNameComparatorDescending);
		}

	}

	public void showUpdateProfesor(int row) {
		
		service.setProf(service.getProfesori().get(row));
		
	}

	public String getNrMatricol() {
		return nrMatricol;
	}

	public void setNrMatricol(String nrMatricol) {
		this.nrMatricol = nrMatricol;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<StudentForm> getS() {
		// if (FacesContext.getCurrentInstance().getRenderResponse()) {
		// init();
		// }
		return s;
	}

	public void setS(List<StudentForm> s) {
		// init();
		this.s = s;
	}

	public List<NewsBean> getContinutNews() {
		return continutNews;
	}

	public void setContinutNews(List<NewsBean> continutNews) {
		this.continutNews = continutNews;
	}

	public List<Catalog> getCatalog() {
		return catalog;
	}

	public void setCatalog(List<Catalog> catalog) {
		this.catalog = catalog;
	}

	public ActionLinkClass getService() {
		return service;
	}

	public void setService(ActionLinkClass service) {
		this.service = service;
	}

	public List<Profesor> getProfesori() {
		return profesori;
	}

	public void setProfesori(List<Profesor> profesori) {
		this.profesori = profesori;
	}

	public List<String> getGrupe() {
		return grupe;
	}

	public void setGrupe(List<String> grupe) {
		this.grupe = grupe;
	}

	public List<CatalogBean> getC1() {
		return c1;
	}

	public void setC1(List<CatalogBean> c1) {
		this.c1 = c1;
	}

}
