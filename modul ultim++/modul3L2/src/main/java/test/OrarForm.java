package test;

public class OrarForm {
	
	private String numeObiect;
	private String data;
	private String interval;
	private String sala;
	
	
	public String getNumeObiect() {
		return numeObiect;
	}
	public void setNumeObiect(String numeObiect) {
		this.numeObiect = numeObiect;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public String getInterval() {
		return interval;
	}
	public void setInterval(String interval) {
		this.interval = interval;
	}
	public String getSala() {
		return sala;
	}
	public void setSala(String sala) {
		this.sala = sala;
	}

}
