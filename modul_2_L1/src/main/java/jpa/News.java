package jpa;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;


@Entity

public class News implements Serializable {

       
    @Id
    @Column(name="ID_NEWS")
    private int idNews;
    private String titlu;
    private String continut;
    @Temporal(TemporalType.DATE)
    @Column(name="DATA_APARITIEI")
    private Date dataAparitiei;
    private static final long serialVersionUID = 1L;

    public News() {
        super();
    }   
    public int getIdNews() {
        return this.idNews;
    }

    public void setIdNews(int idNews) {
        this.idNews = idNews;
    }   
    public String getTitlu() {
        return this.titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }   
    public String getContinut() {
        return this.continut;
    }

    public void setContinut(String continut) {
        this.continut = continut;
    }   
    public Date getDataAparitiei() {
        return this.dataAparitiei;
    }

    public void setDataAparitiei(Date dataAparitiei) {
        this.dataAparitiei = dataAparitiei;
    }
   
}
