package jpa;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Evaluare
 *
 */
@Entity

public class Evaluare implements Serializable {

       
    @Id
    @Column(name="ID_EVALUARE")
    private int idEvaluare;
    @Column(name="TIP_EVALUARE")
    private String tipEvaluare;
    private static final long serialVersionUID = 1L;

    public Evaluare() {
        super();
    }   
    public int getIdEvaluare() {
        return this.idEvaluare;
    }

    public void setIdEvaluare(int idEvaluare) {
        this.idEvaluare = idEvaluare;
    }   
    public String getTipEvaluare() {
        return this.tipEvaluare;
    }

    public void setTipEvaluare(String tipEvaluare) {
        this.tipEvaluare = tipEvaluare;
    }
   
}
