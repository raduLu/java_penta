package jpa;

import java.io.Serializable;
import javax.persistence.*;



@Entity
public class Grupa implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name="ID_GRUPA")
    private String idGrupa;

    @Column(name="NUME_GRUPA")
    private String numeGrupa;

    public Grupa() {
    }

    public String getIdGrupa() {
        return this.idGrupa;
    }

    public void setIdGrupa(String idGrupa) {
        this.idGrupa = idGrupa;
    }

    public String getNumeGrupa() {
        return this.numeGrupa;
    }

    public void setNumeGrupa(String numeGrupa) {
        this.numeGrupa = numeGrupa;
    }

}