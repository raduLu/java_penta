package jpa;

import java.io.Serializable;

import java.util.Date;
import javax.persistence.*;


@Entity

public class Orar implements Serializable {

       
    @Id
    @Column(name="ID_OBIECT")
    private int idObiect;
    @Column(name="ID_GRUPA")
    private int idGrupa;
    @Temporal(TemporalType.DATE)
    @Column(name="DATA")
    private Date data;
    @Column(name="INTERVAL_ORAR")
    private String intervalOrar;
    @Column(name="SALA_CURS")
    private int salaCurs;
    
    private static final long serialVersionUID = 1L;

    public Orar() {
        super();
    }   
    public int getIdObiect() {
        return this.idObiect;
    }

    public void setIdObiect(int idObiect) {
        this.idObiect = idObiect;
    }   
    public int getIdGrupa() {
        return this.idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }   
    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }   
    public String getIntervalOrar() {
        return this.intervalOrar;
    }

    public void setIntervalOrar(String intervalOrar) {
        this.intervalOrar = intervalOrar;
    }   
    public int getSalaCurs() {
        return this.salaCurs;
    }

    public void setSalaCurs(int salaCurs) {
        this.salaCurs = salaCurs;
    }
   
}
