package jpa;

import java.io.Serializable;

import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Obiect
 *
 */
@Entity

public class Obiect implements Serializable {

       
    @Id
    @Column(name="ID_OBIECT")
    private int idObiect;
    @Column(name="ID_PROFESOR")
    private int idProfesor;
    @Column(name="ID_EVALUARE")
    private int idEvaluare;
    private String nume;
    private int credite;
    private List<String> prezenta;
    private static final long serialVersionUID = 1L;

    public Obiect() {
        super();
    }   
    public int getIdObiect() {
        return this.idObiect;
    }

    public void setIdObiect(int idObiect) {
        this.idObiect = idObiect;
    }   
    public int getIdProfesor() {
        return this.idProfesor;
    }

    public void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }   
    public int getIdEvaluare() {
        return this.idEvaluare;
    }

    public void setIdEvaluare(int idEvaluare) {
        this.idEvaluare = idEvaluare;
    }   
    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }   
    public int getCredite() {
        return this.credite;
    }

    public void setCredite(int credite) {
        this.credite = credite;
    }   
    public List<String> getPrezenta() {
        return this.prezenta;
    }

    public void setPrezenta(List<String> prezenta) {
        this.prezenta = prezenta;
    }
   
}
