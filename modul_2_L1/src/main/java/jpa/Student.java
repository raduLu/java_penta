package jpa;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
/**
 * Entity implementation class for Entity: Student
 *
 */
@Entity

public class Student implements Serializable {

       
    @Id
    @Column (name="ID_STUDENT")
    @NotNull
    private int idStudent;
    @Column (name="ID_GRUPA")
    private int idGrupa;
    @NotNull
    @Column (name="NUME")
    private String nume;
    @Column (name="PRENUME")
    @NotNull
    private String prenume;
    @Column (name="AN_STUDIU")
    private int anStudiu;
    @Column (name="NR_MATRICOL")
    @NotNull
    private int nrMatricol;
    private String email;
    private static final long serialVersionUID = 1L;

    public Student() {
        super();
    }   
    public int getIdStudent() {
        return this.idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }   
    public int getIdGrupa() {
        return this.idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }   
    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }   
    public String getPrenume() {
        return this.prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }   
    public int getAnStudiu() {
        return this.anStudiu;
    }

    public void setAnStudiu(int anStudiu) {
        this.anStudiu = anStudiu;
    }   
    public int getNrMatricol() {
        return this.nrMatricol;
    }

    public void setNrMatricol(int nrMatricol) {
        this.nrMatricol = nrMatricol;
    }   
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
   
}
