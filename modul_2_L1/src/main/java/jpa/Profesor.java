package jpa;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Profesor
 *
 */
@Entity

public class Profesor implements Serializable {

       
    @Id
    @Column(name="ID_PROFESOR")
    private int idProfesor;
    private String nume;
    private String prenume;
    private String email;
    @Column(name="SITE_PERSONAL")
    private String sitePersonal;
    private static final long serialVersionUID = 1L;

    public Profesor() {
        super();
    }   
    public int getIdProfesor() {
        return this.idProfesor;
    }

    public void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }   
    public String getNume() {
        return this.nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }   
    public String getPrenume() {
        return this.prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }   
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }   
    public String getSitePersonal() {
        return this.sitePersonal;
    }

    public void setSitePersonal(String sitePersonal) {
        this.sitePersonal = sitePersonal;
    }
   
}
